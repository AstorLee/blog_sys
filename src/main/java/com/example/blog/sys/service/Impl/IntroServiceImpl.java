package com.example.blog.sys.service.Impl;

import com.example.blog.sys.dao.Intro;
import com.example.blog.sys.repository.IntroRepository;
import com.example.blog.sys.service.IntroService;
import com.example.blog.sys.vo.AuthorVO;
import com.example.blog.sys.vo.IntroVO;
import com.example.blog.sys.vo.PageVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/26 16:57
 **/
@Service
public class IntroServiceImpl implements IntroService {

    @Autowired
    private IntroRepository repository;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Intro saveIntro(Intro intro) {
        return repository.save(intro);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean delIntro(String introId) {
        repository.deleteById(introId);
        return true;
    }

    @Override
    public Boolean delByArticleId(String articleId) {
        return repository.deleteByArticleId(articleId);
    }

    @Override
    public List<IntroVO> findAll() {

        List<Intro> introList  = repository.findAll();
        List<IntroVO> introVOList = new ArrayList<>();
        for(Intro intro:introList) {
            IntroVO introVO = new IntroVO();
            BeanUtils.copyProperties(intro,introVO);
            introVOList.add(introVO);
        }
        return introVOList;
    }

    @Override
    public PageVO findAllByThemeId(Integer themeId, Pageable pageable) {
        PageVO pageVO = new PageVO();
        Page<Intro> introPage  = repository.findAllByThemeId(themeId,pageable);
        List<Intro> introList = introPage.getContent();
        List<IntroVO> introVOList = new ArrayList<>();
        for(Intro intro:introList) {
            IntroVO introVO = new IntroVO();
            BeanUtils.copyProperties(intro,introVO);
            introVOList.add(introVO);
        }
        pageVO.setTotal(introPage.getSize());
        pageVO.setList(introVOList);
        pageVO.setOffset(introPage.getPageable().getPageNumber() + 1);
        pageVO.setLimit(introPage.getPageable().getPageSize());
        return pageVO;
    }

    @Override
    public Intro findByArticleId(String articleId) {
        return repository.findByArticleId(articleId);
    }

    @Override
    public List<IntroVO> findAllByUserId(String userId) {
        List<Intro> introList  = repository.findAllByUserId(userId);
        List<IntroVO> introVOList = new ArrayList<>();
        for(Intro intro:introList) {
            IntroVO introVO = new IntroVO();
            BeanUtils.copyProperties(intro,introVO);
            introVOList.add(introVO);
        }
        return introVOList;
    }

    @Override
    public Intro findById(String id) {
        return repository.findByIntroId(id);
    }

    @Override
    public PageVO findByRangeId(Integer rangeId, Pageable pageable) {
        Page<Intro> introPage = repository.findAllByRangeId(rangeId,pageable);
        List<IntroVO> introVOList = introPage.getContent().stream().map( intro -> {
            IntroVO introVO = new IntroVO();
            BeanUtils.copyProperties(intro,introVO);
            return introVO;
        } ).collect(Collectors.toList());
        PageVO pageVO = new PageVO();
        pageVO.setTotal(introPage.getSize());
        pageVO.setList(introVOList);
        pageVO.setOffset(introPage.getPageable().getPageNumber() + 1);
        pageVO.setLimit(introPage.getPageable().getPageSize());
        return pageVO;
    }

    @Override
    public List<IntroVO> findAllByFollow(String userId) {
        return repository.findAllByFollowFromId(userId).stream().map( intro -> {
            IntroVO introVO = new IntroVO();
            BeanUtils.copyProperties(intro,introVO);
            return introVO;
        } ).collect(Collectors.toList());
    }

    /*@Override
    public PageVO findAllByHot(Pageable pageable) {
        Page<Intro> introPage = repository.findAllByOrOrderByIntroLike(pageable);
        List<IntroVO> introVOList = introPage.getContent().stream().map( intro -> {
            IntroVO introVO = new IntroVO();
            BeanUtils.copyProperties(intro,introVO);
            return introVO;
        } ).collect(Collectors.toList());
        PageVO pageVO = new PageVO();
        pageVO.setTotal(introPage.getSize());
        pageVO.setList(introVOList);
        pageVO.setOffset(introPage.getPageable().getPageNumber() + 1);
        pageVO.setLimit(introPage.getPageable().getPageSize());
        return pageVO;
    }*/

}
