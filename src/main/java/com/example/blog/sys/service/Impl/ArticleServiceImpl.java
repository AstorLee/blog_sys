package com.example.blog.sys.service.Impl;

import com.example.blog.sys.dao.Article;
import com.example.blog.sys.repository.ArticleRepository;
import com.example.blog.sys.service.ArticleService;
import com.example.blog.sys.vo.IntroVO;
import com.example.blog.sys.vo.save.ArticleSaveVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.List;

/**
 * @Author: LetBeing
 * @Description : 文章管理现实类
 * @Date: 2019/8/26 15:44
 **/
@Service
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    private ArticleRepository repository;

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public Article saveArticle(Article article) {
        return repository.save(article);
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public Boolean delArticle(String id) {
        try {
            repository.deleteById(id);
        } catch (RuntimeException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public Article findByArticleId(String articleId) {
        return repository.findByArticleId(articleId);
    }
}
