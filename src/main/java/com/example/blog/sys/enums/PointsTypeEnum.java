package com.example.blog.sys.enums;

import lombok.Getter;

/**
 * @Author: LetBeing
 * @Description : 积分类型enum
 * @Date: 2019/8/22 17:14
 **/

@Getter
public enum PointsTypeEnum {

    /**
     * @ADD 为增
     * @SUBTRACT 为减
     * */

    ADD(1),SUBTRACT(0);

    private final Integer type;

    PointsTypeEnum(Integer type) {
        this.type = type;
    }
}
