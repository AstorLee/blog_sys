package com.example.blog.sys.service;

import com.example.blog.sys.dao.Like;
import com.example.blog.sys.vo.save.LikeSaveVO;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/29 10:37
 **/

public interface LikeService {

    Integer countLikeTo(String articleId,String likeType);

    Boolean saveLike(Like like);

    Boolean delLike(String likeId);

    Like findByLikeFrom(String formId);

    Like findByLikeFromAndTo(String formId,String to);

    Like findBuLikeId(String likeId);
}
