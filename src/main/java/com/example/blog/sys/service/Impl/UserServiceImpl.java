package com.example.blog.sys.service.Impl;

import com.example.blog.sys.dao.User;
import com.example.blog.sys.enums.UserStatusEnum;
import com.example.blog.sys.repository.UserRepository;
import com.example.blog.sys.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/22 17:56
 **/

@Service
public class UserServiceImpl implements UserService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    JavaMailSenderImpl mailSender;

    @Autowired
    UserRepository userRepository;

    @Value("${spring.mail.from}")
    private String from;

    @Override
    public User findByUserAccount(String account) {
        return userRepository.findByUserAccount(account);
    }

    @Override
    public User findByUserId(String userId) {
        return userRepository.findByUserId(userId);
    }

    @Override
    public User findByUserEmail(String email) {
        return userRepository.findByUserEmail(email);
    }

    @Override
    public User findByUserPhone(Integer phone) {
        return userRepository.findByUserPhone(phone);
    }

    @Override
    public User userSave(User user) {
        return userRepository.save(user);
    }

    @Override
    @Transactional
    public Boolean userActivate(String id, String code) throws RuntimeException {
        User user = userRepository.findByUserId(id);
        if ( user != null && code != null) {
            if ( user.getUserCode().equals(code) ) {
                user.setUserStatus(UserStatusEnum.START.getStatus());
                user.setUserCode("");
                userRepository.save(user);
                return true;
            }else {
                return false;
            }
        }else {
            throw new RuntimeException("找不到该用户或验证码为空");
        }
    }


    /**
     * @params to 收件人
     * @params subject 主题
     * @params content 内容
     */
    @Override
    public Boolean activateEmailUser(String to, String subject, String content) {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = null;
        try {
            helper = new MimeMessageHelper(message, true);
            helper.setFrom(from);
            helper.setSubject(subject);
            helper.setTo(to);
            helper.setText(content, true);
            mailSender.send(message);
            logger.info("邮件发送成功");
        } catch (MessagingException e) {
            logger.error("发送邮件时发生异常", e);
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
