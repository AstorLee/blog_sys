package com.example.blog.sys.service;

import com.example.blog.sys.dao.UserInfo;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/23 16:26
 **/

public interface UserInfoService {

    public UserInfo findByUserId(String userId);

    public UserInfo findByInfoId(String infoId);

    UserInfo updateUserInfo(UserInfo userInfo);

    UserInfo saveUserInfo(UserInfo userInfo);
}
