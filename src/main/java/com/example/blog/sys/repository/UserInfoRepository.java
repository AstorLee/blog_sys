package com.example.blog.sys.repository;

import com.example.blog.sys.dao.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/22 17:42
 **/

public interface UserInfoRepository extends JpaRepository<UserInfo,String> {

    UserInfo findByUserId(String userId);

    UserInfo findByInfoId(String infoId);
}
