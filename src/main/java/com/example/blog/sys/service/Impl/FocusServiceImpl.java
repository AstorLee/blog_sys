package com.example.blog.sys.service.Impl;

import com.example.blog.sys.dao.Focus;
import com.example.blog.sys.repository.FocusRepository;
import com.example.blog.sys.service.FocusService;
import com.example.blog.sys.vo.FocusVO;
import com.example.blog.sys.vo.save.FocusSaveVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/2 18:12
 **/

@Service
public class FocusServiceImpl implements FocusService {

    @Autowired
    private FocusRepository repository;

    @Override
    public List<FocusVO> findAllByUserId(String userId) {
        List<Focus> focusList = repository.findAllByUserId(userId);
        List<FocusVO> focusVOList = new ArrayList<FocusVO>();
        for( Focus focus:focusList ) {
            FocusVO focusVO = new FocusVO();
            BeanUtils.copyProperties(focus,focusVO);
            focusVOList.add(focusVO);
        }
        return focusVOList;
    }

    @Override
    public List<FocusVO> findAllByTopicId(Integer topicId) {
        List<Focus> focusList = repository.findAllByTopicId(topicId);
        List<FocusVO> focusVOList = new ArrayList<FocusVO>();
        for( Focus focus:focusList ) {
            FocusVO focusVO = new FocusVO();
            BeanUtils.copyProperties(focus,focusVO);
            if ( focus.getFocusImgs() != null && !"".equals(focus.getFocusImgs()) ) {
                String[] imgArr = focus.getFocusImgs().split(",");
                focusVO.setImgList(imgArr);
            }else {
                focusVO.setImgList(new String[]{});
            }
            focusVOList.add(focusVO);
        }
        return focusVOList;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveFocus(Focus focus) {
        try {
            repository.save(focus);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean delFocus(String focusId) {
        try {
            repository.deleteById(focusId);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public Focus findByFocusId(String focusId) {
        return repository.findByFocusId(focusId);
    }
}
