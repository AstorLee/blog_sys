package com.example.blog.sys.controller;

import com.example.blog.sys.conf.MyException;
import com.example.blog.sys.dao.User;
import com.example.blog.sys.dao.UserInfo;
import com.example.blog.sys.enums.ErrorCodeEnum;
import com.example.blog.sys.note.RequireToken;
import com.example.blog.sys.service.Impl.RedisTemplateService;
import com.example.blog.sys.service.UserInfoService;
import com.example.blog.sys.service.UserService;
import com.example.blog.sys.vo.ResultVO;
import com.example.blog.sys.vo.UserInfoVO;
import com.example.blog.sys.vo.save.UserInfoSaveVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.xml.transform.Result;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019-08-22 10:00
 **/

@RestController
@RequestMapping("/user")
@Api(value = "user API", tags = "用户管理")
public class UserController {

    @Resource
    UserService userService;

    @Autowired
    UserInfoService userInfoService;

    @Autowired
    RedisTemplateService redisTemplateService;

    @ApiOperation(value = "查询用户详情信息")
    @GetMapping("")
    @ApiImplicitParam(name = "id", value = "用户id", paramType = "query")
    public ResultVO findToUserInfo(@RequestParam(name = "id") String userId) {
        UserInfo userInfo = userInfoService.findByUserId(userId);
        UserInfoVO userInfoVO = new UserInfoVO();
        BeanUtils.copyProperties(userInfo,userInfoVO);
        return new ResultVO("成功", 1000,userInfoVO);
    }

    @ApiOperation(value = "更新用户信息")
    @PostMapping("")
    @RequireToken
    public ResultVO updateUserInfo(@RequestBody UserInfoSaveVO userInfoSaveVO, HttpServletRequest request) {

        String key = "user_" + request.getHeader("jwt");
        User user = redisTemplateService.get(key, User.class);
        if (user != null) {
            UserInfo userInfo = userInfoService.findByUserId(user.getUserId());
            if (user.getUserId().equals(userInfo.getUserId())) {
                userInfo.setInfoNickname(userInfoSaveVO.getInfoNickname());
                userInfo.setInfoAvatar(userInfoSaveVO.getInfoAvatar());
                userInfo.setInfoDescription(userInfoSaveVO.getInfoDescription());
                UserInfo newUserInfo = userInfoService.saveUserInfo(userInfo);
                return new ResultVO("成功", 1000, newUserInfo);
            } else {
                throw new MyException(ErrorCodeEnum.USERERR.getCode(),"非法操作");
            }
        }else {
            throw new MyException(ErrorCodeEnum.USERERR.getCode(),"非法操作");
        }
    }

}
