package com.example.blog.sys.dao;

import lombok.Data;

import javax.persistence.*;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/3 10:12
 **/

@Entity
@Data
@Table(name = "blog_sys_user_role")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer roleId;

    private String roleName;

    private Integer roleCode = 10;
}
