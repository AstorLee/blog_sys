package com.example.blog.sys.vo.save;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/3 16:26
 **/

@Data
public class FollowSaveVO {

    @JsonProperty("to")
    @NotEmpty(message = "目标编号不能为空")
    private String followTo;

    @JsonProperty("type")
    @NotEmpty(message = "类型不能为空")
    private String followType;

}
