/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 80015
 Source Host           : localhost:3306
 Source Schema         : blog_sys

 Target Server Type    : MySQL
 Target Server Version : 80015
 File Encoding         : 65001

 Date: 28/02/2020 10:22:45
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for blog_sys_advertising
-- ----------------------------
DROP TABLE IF EXISTS `blog_sys_advertising`;
CREATE TABLE `blog_sys_advertising` (
  `advertising_id` int(11) NOT NULL AUTO_INCREMENT,
  `advertising_xyz` int(11) DEFAULT NULL,
  `advertising_link` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `advertising_banner` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`advertising_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='广告';

-- ----------------------------
-- Table structure for blog_sys_art_ranking
-- ----------------------------
DROP TABLE IF EXISTS `blog_sys_art_ranking`;
CREATE TABLE `blog_sys_art_ranking` (
  `art_ranking_id` int(11) NOT NULL,
  `art_ranking_sort` int(11) DEFAULT NULL,
  `intro_id` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `art_ranking_create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`art_ranking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='文章排行榜';

-- ----------------------------
-- Table structure for blog_sys_article
-- ----------------------------
DROP TABLE IF EXISTS `blog_sys_article`;
CREATE TABLE `blog_sys_article` (
  `article_id` char(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `article_title` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `article_content` longtext COLLATE utf8mb4_unicode_ci,
  `article_create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `article_update_time` timestamp NULL DEFAULT NULL,
  `intro_id` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `theme_id` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `theme_name` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `special_id` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `article_look` int(11) DEFAULT '0' COMMENT '文章查看数',
  `article_collect` int(11) DEFAULT '0' COMMENT '文章收藏数',
  `article_comment` int(11) DEFAULT '0' COMMENT '文章评论数',
  `article_like` int(11) DEFAULT '0' COMMENT '文章点赞数',
  PRIMARY KEY (`article_id`),
  KEY `intro_id` (`intro_id`),
  CONSTRAINT `intro_id` FOREIGN KEY (`intro_id`) REFERENCES `blog_sys_article_intro` (`intro_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='文章主表';

-- ----------------------------
-- Table structure for blog_sys_article_intro
-- ----------------------------
DROP TABLE IF EXISTS `blog_sys_article_intro`;
CREATE TABLE `blog_sys_article_intro` (
  `intro_id` char(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `intro_title` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `intro_description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `intro_look` int(11) DEFAULT '0' COMMENT '浏览量',
  `intro_like` int(11) DEFAULT '0',
  `intro_comment` int(11) DEFAULT '0',
  `intro_collect` int(11) DEFAULT '0' COMMENT '收藏数',
  `intro_create_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `intro_update_time` timestamp NULL DEFAULT NULL,
  `intro_thumbnail` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '缩略图',
  `article_id` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `theme_id` int(11) DEFAULT NULL,
  `user_id` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_nickname` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `range_id` int(11) DEFAULT NULL,
  `range_name` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `theme_name` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`intro_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='文章从表';

-- ----------------------------
-- Table structure for blog_sys_comment
-- ----------------------------
DROP TABLE IF EXISTS `blog_sys_comment`;
CREATE TABLE `blog_sys_comment` (
  `comment_id` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_content` longtext COLLATE utf8mb4_unicode_ci,
  `comment_create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `comment_from` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_to` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_to_user_id` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment_to_nickname` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment_type` enum('1','2','3') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '1' COMMENT '1为评论文章\n2为回复评论\n3为评论看点',
  `comment_sid` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '评论上级id',
  `article_id` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `focus_id` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment_from_nickname` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment_from_avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='评论表';

-- ----------------------------
-- Table structure for blog_sys_focus
-- ----------------------------
DROP TABLE IF EXISTS `blog_sys_focus`;
CREATE TABLE `blog_sys_focus` (
  `focus_id` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `focus_content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `focus_create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `topic_name` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `topic_id` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_nickname` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment_num` int(11) DEFAULT NULL,
  `like_num` int(11) DEFAULT NULL,
  `focus_imgs` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`focus_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='看点';

-- ----------------------------
-- Table structure for blog_sys_like
-- ----------------------------
DROP TABLE IF EXISTS `blog_sys_like`;
CREATE TABLE `blog_sys_like` (
  `like_id` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `like_from` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `like_to` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `like_to_user_id` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `like_type` enum('1','2') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '1' COMMENT '1为对文章，2位对用户评论',
  `like_createTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`like_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='点赞表';

-- ----------------------------
-- Table structure for blog_sys_range
-- ----------------------------
DROP TABLE IF EXISTS `blog_sys_range`;
CREATE TABLE `blog_sys_range` (
  `range_id` int(11) NOT NULL AUTO_INCREMENT,
  `range_name` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`range_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='范围表';

-- ----------------------------
-- Table structure for blog_sys_ranking
-- ----------------------------
DROP TABLE IF EXISTS `blog_sys_ranking`;
CREATE TABLE `blog_sys_ranking` (
  `ranking_id` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ranking_type` enum('1','2','3') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '1' COMMENT '1为作者榜\n2为热门榜\n3最新',
  `user_id` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ranking_sort` int(11) DEFAULT NULL,
  `ranking_create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ranking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='排行榜';

-- ----------------------------
-- Table structure for blog_sys_theme
-- ----------------------------
DROP TABLE IF EXISTS `blog_sys_theme`;
CREATE TABLE `blog_sys_theme` (
  `theme_id` int(11) NOT NULL AUTO_INCREMENT,
  `theme_name` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `theme_createTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `theme_num` int(11) DEFAULT '0',
  `range_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`theme_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='主题表';

-- ----------------------------
-- Table structure for blog_sys_topic
-- ----------------------------
DROP TABLE IF EXISTS `blog_sys_topic`;
CREATE TABLE `blog_sys_topic` (
  `topic_id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_name` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `topic_icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `topic_create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `follow_num` int(11) DEFAULT '0',
  `focus_num` int(11) DEFAULT '0',
  PRIMARY KEY (`topic_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='话题表';

-- ----------------------------
-- Table structure for blog_sys_user
-- ----------------------------
DROP TABLE IF EXISTS `blog_sys_user`;
CREATE TABLE `blog_sys_user` (
  `user_id` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_account` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_pwd` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info_id` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_email` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_phone` bigint(12) DEFAULT NULL,
  `role_id` int(11) DEFAULT '2',
  `role_name` char(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_code` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '注册，修改验证码',
  `user_status` enum('0','1') COLLATE utf8mb4_unicode_ci DEFAULT '0' COMMENT '用户状态，已激活|未激活|已封禁|正常',
  `user_nickname` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户主表';

-- ----------------------------
-- Table structure for blog_sys_user_cate
-- ----------------------------
DROP TABLE IF EXISTS `blog_sys_user_cate`;
CREATE TABLE `blog_sys_user_cate` (
  `cate_id` int(11) NOT NULL AUTO_INCREMENT,
  `cate_name` char(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `article_num` int(11) DEFAULT NULL,
  `user_id` char(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`cate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户分类表';

-- ----------------------------
-- Table structure for blog_sys_user_collect
-- ----------------------------
DROP TABLE IF EXISTS `blog_sys_user_collect`;
CREATE TABLE `blog_sys_user_collect` (
  `collect_id` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `collect_from` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collect_to` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collect_to_user_id` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collect_create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `cate_id` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`collect_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户收藏表';

-- ----------------------------
-- Table structure for blog_sys_user_collect_cate
-- ----------------------------
DROP TABLE IF EXISTS `blog_sys_user_collect_cate`;
CREATE TABLE `blog_sys_user_collect_cate` (
  `cate_id` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cate_name` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cate_create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `cate_num` int(11) DEFAULT NULL,
  `user_id` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cate_icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '取最新文章的icon',
  PRIMARY KEY (`cate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户收藏分类';

-- ----------------------------
-- Table structure for blog_sys_user_events
-- ----------------------------
DROP TABLE IF EXISTS `blog_sys_user_events`;
CREATE TABLE `blog_sys_user_events` (
  `events_id` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `events_type` enum('1','2','3','4','5') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '1关注的人更新，2被关注，3被点赞，4被收藏，5被评论',
  `events_from` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `events_to` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `events_status` enum('1','2') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '1为已读\n2为未读',
  `events_create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `events_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '动态描述',
  `events_touch` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '触发动态的点',
  PRIMARY KEY (`events_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户动态表';

-- ----------------------------
-- Table structure for blog_sys_user_follow
-- ----------------------------
DROP TABLE IF EXISTS `blog_sys_user_follow`;
CREATE TABLE `blog_sys_user_follow` (
  `follow_id` char(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `follow_from` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `follow_to` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `follow_type` enum('1','2','3','4') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '关注类型，1：用户，2：文章，3：专栏，4：话题',
  `follow_createTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`follow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户关注表';

-- ----------------------------
-- Table structure for blog_sys_user_info
-- ----------------------------
DROP TABLE IF EXISTS `blog_sys_user_info`;
CREATE TABLE `blog_sys_user_info` (
  `info_id` char(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `info_nickname` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info_avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info_create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `article_num` int(11) DEFAULT '0' COMMENT '文章数',
  `cate_num` int(11) DEFAULT '0' COMMENT '分类数',
  `special_num` int(11) DEFAULT '0' COMMENT '专栏数',
  `follow_num` int(11) DEFAULT '0' COMMENT '粉丝数',
  `to_follow_num` int(11) DEFAULT '0' COMMENT '关注数',
  `collect_num` int(11) DEFAULT '0',
  PRIMARY KEY (`info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户从表';

-- ----------------------------
-- Table structure for blog_sys_user_points
-- ----------------------------
DROP TABLE IF EXISTS `blog_sys_user_points`;
CREATE TABLE `blog_sys_user_points` (
  `points_id` char(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `points_num` int(11) DEFAULT NULL,
  `points_type` enum('1','2','3','4') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '1',
  `points_createTime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `user_id` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`points_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户积分表';

-- ----------------------------
-- Table structure for blog_sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `blog_sys_user_role`;
CREATE TABLE `blog_sys_user_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` char(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_code` int(11) DEFAULT NULL COMMENT '权限码：10为普通用户，20位管理员',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户角色表';

-- ----------------------------
-- Table structure for blog_sys_user_settings
-- ----------------------------
DROP TABLE IF EXISTS `blog_sys_user_settings`;
CREATE TABLE `blog_sys_user_settings` (
  `settings_id` char(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `setting_show_email` enum('0','1') COLLATE utf8mb4_unicode_ci DEFAULT '1',
  `user_id` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`settings_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户设置表';

-- ----------------------------
-- Table structure for blog_sys_user_special
-- ----------------------------
DROP TABLE IF EXISTS `blog_sys_user_special`;
CREATE TABLE `blog_sys_user_special` (
  `special_id` char(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `special_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `article_num` int(11) DEFAULT '0' COMMENT '文章数',
  `follow_num` int(11) DEFAULT '0' COMMENT '关注数',
  `special_icon` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '缩略图',
  `look_num` int(11) DEFAULT '0' COMMENT '访问量',
  `user_id` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`special_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户专栏表';

SET FOREIGN_KEY_CHECKS = 1;
