package com.example.blog.sys.vo.save;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class UserSaveVO {

    @JsonProperty("account")
    @NotEmpty(message = "账号不能为空")
    private String userAccount;

    @JsonProperty("pwd")
    @NotEmpty(message = "密码不能为空")
    private String userPwd;

    @JsonProperty("email")
    @NotEmpty(message = "邮箱不能为空")
    private String userEmail;

    @JsonProperty("nickName")
    private String userNickname;

}
