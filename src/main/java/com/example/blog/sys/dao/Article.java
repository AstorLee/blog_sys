package com.example.blog.sys.dao;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.util.List;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019-08-22 11:42
 **/

@Entity
@Table(name = "blog_sys_article")
@Data
@GenericGenerator(name = "jpa-uuid",strategy = "uuid")
public class Article {

    @Id
    @GeneratedValue(generator = "jpa-uuid",strategy = GenerationType.IDENTITY)
    @Column(length = 32)
    private String articleId;

    @NotEmpty(message = "文章标题不能为空")
    @Size(min = 1,max = 20)
    private String articleTitle;

    @NotEmpty(message = "文章内容不能为空")
    private String articleContent;

    private String introId;

    private Integer themeId;

    private String themeName;

    private String specialId;

    @NotEmpty(message = "用户编号不能为空")
    private String userId;

    private Timestamp articleCreateTime = new Timestamp(System.currentTimeMillis());

    private Integer articleLook = 0;

    private Integer articleCollect = 0;

    private Integer articleLike = 0;

    private Integer articleComment = 0;

}
