package com.example.blog.sys.repository;

import com.example.blog.sys.dao.Follow;
import com.example.blog.sys.vo.UserVO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/30 14:53
 **/

@Repository
public interface FollowRepository extends JpaRepository<Follow,String> {

    List<Follow> findAllByFollowTo(String toId);

    Boolean deleteByFollowFromAndFollowTo(String fromId,String toId);

    List<Follow> findAllByFollowFromAndFollowType(String fromId,String type);

    List<Follow> findAllByFollowType(String type);

    Follow findByFollowId(String followId);

    Follow findByFollowFromAndFollowTo(String from,String to);

}
