package com.example.blog.sys.controller;

import com.example.blog.sys.conf.MyException;
import com.example.blog.sys.dao.User;
import com.example.blog.sys.dao.UserInfo;
import com.example.blog.sys.enums.ErrorCodeEnum;
import com.example.blog.sys.repository.UserInfoRepository;
import com.example.blog.sys.service.Impl.RedisTemplateService;
import com.example.blog.sys.service.UserInfoService;
import com.example.blog.sys.service.UserService;
import com.example.blog.sys.util.JwtUtils;
import com.example.blog.sys.vo.LoginVO;
import com.example.blog.sys.vo.ResultVO;
import com.example.blog.sys.vo.UserInfoVO;
import com.example.blog.sys.vo.UserVO;
import com.example.blog.sys.vo.save.UserSaveVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;


/**
 * @Author: LetBeing
 * @Description : 公共方法
 * @Date: 2019-08-22 10:49
 **/

@RestController
@Api(value = "公共方法",tags = "公共方法")
public class CommonController {

    @Resource
    UserService userService;

    @Resource
    UserInfoRepository userInfoRepository;

    @Resource
    UserInfoService userInfoService;

    @Autowired
    RedisTemplateService redisTemplateService;

    @ApiOperation(value = "用户登录",notes = "用户登录")
    @PostMapping("/login")
    public ResultVO userLogin(@RequestBody LoginVO loginVO, HttpServletResponse response) {
        User user = userService.findByUserAccount(loginVO.getUserAccount());
        ResultVO resultVO = new ResultVO();

        if ( user != null ) {
            String pwd = user.getUserPwd();
            String originPwd = loginVO.getUserPwd();
            //截取后12位作为密码
            if ( originPwd.length() > 20 ) {
                String newOriginPwd = originPwd.substring(20);

                if ( user == null ) {
                    throw new MyException(ErrorCodeEnum.NOTEXIST.getCode(),"用户不存在");
                }else if ( !user.getUserPwd().equals(newOriginPwd) ){
                    throw new MyException(ErrorCodeEnum.USERERR.getCode(),"密码不正确");
                }else if ("0".equals(user.getUserStatus())) {
                    throw new MyException(ErrorCodeEnum.USERERR.getCode(),"账号未激活");
                } else {
                    UserVO userVO = new UserVO();
                    UserInfoVO userInfoVO = new UserInfoVO();
                    BeanUtils.copyProperties(user,userVO);
                    UserInfo userInfo = userInfoRepository.findByInfoId(user.getInfoId());
                    BeanUtils.copyProperties(userInfo,userInfoVO);
                    userVO.setInfoAvatar(userInfoVO.getInfoAvatar());
                    userVO.setInfoDescription(userInfo.getInfoDescription());
                    userVO.setInfoNickname(userInfo.getInfoNickname());
                    userVO.setArticleNum(userInfo.getArticleNum());
                    userVO.setCateNum(userInfo.getCateNum());
                    userVO.setSpecialNum(userInfo.getSpecialNum());
                    //设置缓存
                    String token = new JwtUtils().getToken(user);
                    String key = "user_" + token;
                    redisTemplateService.set(key,user);
                    //设置响应头
                    response.setHeader("jwt",token);
                    //响应
                    resultVO.setCode(1000);
                    resultVO.setMessage("成功");
                    resultVO.setData(userVO);
                }
            }else {
                resultVO.setCode(1002);
                resultVO.setMessage("密码不正确");
                resultVO.setData(false);
            }
            return resultVO;
        }else {
            throw new MyException(ErrorCodeEnum.NOTEXIST.getCode(),"用户不存在");
        }
    }

    @ApiOperation(value = "注册账号")
    @PostMapping("/sign")
    public ResultVO sign(@RequestBody UserSaveVO userSaveVO) {
        ResultVO resultVO = new ResultVO();
        //验证是否重复，避免跳过前端的验证
        User validUserAccount = userService.findByUserAccount(userSaveVO.getUserAccount());
        User validUserEmail = userService.findByUserEmail(userSaveVO.getUserEmail());
        /*if ( validUserAccount != null || validUserEmail != null) {
            throw new MyException(1004,"账号或邮箱重复");
        }*/
        User user = new User();
        BeanUtils.copyProperties(userSaveVO, user);
        String uuid = UUID.randomUUID().toString().replace("-", "");
        user.setUserCode(uuid);
        user.setUserPwd(user.getUserPwd().substring(20));
        User newUser = userService.userSave(user);
        UserInfo info = new UserInfo();
        info.setUserId(newUser.getUserId());
        info.setInfoNickname(userSaveVO.getUserNickname());
        UserInfo newUserInfo = userInfoRepository.save(info);
        //补充infoId
        newUser.setInfoId(newUserInfo.getInfoId());
        userService.userSave(newUser);
        String url = String.format("http://localhost:1234/activate/%s/%s", user.getUserId(), uuid);
        String content = String.format("<p>今日论论坛账号激活链接，若非本人操作，请忽略</p><br/><a href='%s'>激活点击此</a>", url);
        Boolean status = userService.activateEmailUser(newUser.getUserEmail(), "【今日论】论坛账号激活", content);
        if (status) {
            resultVO.setCode(1000);
            resultVO.setMessage("成功");
            resultVO.setData(true);
        } else {
            resultVO.setCode(1001);
            resultVO.setMessage("失败");
            resultVO.setData(false);
        }
        return resultVO;
    }

    @ApiOperation(value = "用户登出",notes = "用户登出")
    @PutMapping("/logout")
    public ResultVO userLogout(HttpServletResponse response) {
        String jwt = response.getHeader("jwt");
        ResultVO resultVO = new ResultVO();
        if ( jwt != null ) {
            String key = "user_" + jwt;
            Boolean status = redisTemplateService.del(key);
            if ( status ) {
                resultVO.setCode(1000);
                resultVO.setMessage("成功");
                resultVO.setData(true);
            }else {
                throw new MyException(ErrorCodeEnum.USERERR.getCode(),"失败，token不正确");
            }
        }else {
            throw new MyException(ErrorCodeEnum.USERERR.getCode(),"失败，无token");
        }
        return resultVO;
    }

    @ApiOperation(value = "修改用户密码",notes = "修改用户密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "account",value = "账号"),
            @ApiImplicitParam(name = "pwd",value = "密码"),
            @ApiImplicitParam(name = "code",value = "验证码")
    })
    @PostMapping("/editpwd")
    public ResultVO editpwd() {

        return null;
    }

    @PostMapping("/editemail")
    public ResultVO editmail() {

        return null;
    }

    @PostMapping("/editphone")
    public ResultVO editphone() {

        return null;
    }

    @ApiOperation(value = "激活账号",notes = "激活账号")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id",value = "用户编号"),
            @ApiImplicitParam(name = "code",value = "验证码")
    })
    @GetMapping("/activate/{id}/{code}")
    public ResultVO userActivate(@PathVariable("id") String id,@PathVariable("code") String code) {
        ResultVO resultVO = new ResultVO();
        Boolean status = userService.userActivate(id, code);
        if (status) {
            resultVO.setCode(1000);
            resultVO.setMessage("认证成功");
            resultVO.setData(true);
        } else {
            throw new MyException(ErrorCodeEnum.OPERATIONERR.getCode(),"认证失败");
        }
        return resultVO;
    }

    @ApiOperation(value = "检测账号是否重复")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "account",value = "账号",paramType = "query",dataType = "String"),
            @ApiImplicitParam(name = "email",value = "邮箱",paramType = "query",dataType = "String")
    })
    @GetMapping("/user/exist")
    public ResultVO isExist(@RequestParam(value = "account",required = false) String account,@RequestParam(value = "email",required = false) String email ) {
        ResultVO resultVO = new ResultVO("成功",1000,false);
        if ( account != null ) {
            User user = userService.findByUserAccount(account);
            if ( user != null ) {
                resultVO.setMessage("账号已重复");
                resultVO.setData(true);
            }
        }else if ( email != null ) {
            User user = userService.findByUserEmail(email);
            if ( user != null ) {
                resultVO.setMessage("邮箱已重复");
                resultVO.setData(true);
            }
        }
        return resultVO;
    }
}
