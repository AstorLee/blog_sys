package com.example.blog.sys.service.Impl;

import com.example.blog.sys.dao.CollectCate;
import com.example.blog.sys.repository.CollectCateRepository;
import com.example.blog.sys.repository.CollectRepository;
import com.example.blog.sys.service.CollectCateService;
import com.example.blog.sys.vo.CollectCateVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: LetBeing
 * @Description : 收藏分类service
 * @Date: 2019/9/2 15:03
 **/

@Service
public class CollectCateServiceImpl implements CollectCateService {

    @Autowired
    private CollectCateRepository repository;

    @Override
    public List<CollectCateVO> findAllByUserId(String userId) {
        List<CollectCate> collectCateList = repository.findAllByUserId(userId);
        List<CollectCateVO> collectCateVOList = new ArrayList<>();
        for (CollectCate collectCate:collectCateList) {
            CollectCateVO collectCateVO = new CollectCateVO();
            BeanUtils.copyProperties(collectCate,collectCateVO);
            collectCateVOList.add(collectCateVO);
        }
        return collectCateVOList;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveCollectCate(CollectCate collectCate) {
        try {
            repository.save(collectCate);
        }catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean delCollectCate(String collectCateId) {
        try {
            repository.deleteById(collectCateId);
        }catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public CollectCate findById(String id) {
        return repository.findByCateId(id);
    }
}
