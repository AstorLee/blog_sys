package com.example.blog.sys.repository;

import com.example.blog.sys.dao.CollectCate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author: LetBeing
 * @Description : 收藏分类
 * @Date: 2019/9/2 14:52
 **/

@Repository
public interface CollectCateRepository extends JpaRepository<CollectCate,String> {

    List<CollectCate> findAllByUserId(String userId);

    CollectCate findByCateId(String id);
}
