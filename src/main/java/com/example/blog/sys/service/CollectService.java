package com.example.blog.sys.service;

import com.example.blog.sys.dao.Collect;
import com.example.blog.sys.vo.CollectVO;

import java.util.List;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/2 14:42
 **/

public interface CollectService {

    List<CollectVO> findAllByCateId(String cateId);

    List<CollectVO> findAllByCollectFrom(String fromId);

    Boolean saveCollect(Collect collect);

    Boolean delCollect(String collectId);

    Collect findById(String id);
}
