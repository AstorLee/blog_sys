package com.example.blog.sys.dao;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/2 18:00
 **/

@Data
@Entity
@Table(name = "blog_sys_topic")
public class Topic {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer topicId;

    @NotEmpty(message = "名称不能为空")
    private String topicName;

    private String topicIcon;

    private Integer followNum = 0;

    private Integer focusNum = 0;

}
