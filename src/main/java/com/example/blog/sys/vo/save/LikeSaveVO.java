package com.example.blog.sys.vo.save;

import com.example.blog.sys.enums.LikeTypeEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/2 16:41
 **/

@Data
public class LikeSaveVO {

    @JsonProperty("toId")
    private String likeTo;

    @JsonProperty("type")
    private String likeType;

    @JsonProperty("toUserId")
    private String likeToUserId;
}
