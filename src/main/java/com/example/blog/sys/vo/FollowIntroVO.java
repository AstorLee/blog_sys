package com.example.blog.sys.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/30 14:46
 **/

@Data
public class FollowIntroVO implements Serializable {

    @JsonProperty("title")
    private String introTitle;

    @JsonProperty("description")
    private String introDescription;

    @JsonProperty("look")
    private Integer introLook;

    @JsonProperty("comment")
    private Integer introComment;

    @JsonProperty("like")
    private Integer introLike;

    @JsonProperty("collect")
    private Integer introCollect;

    @JsonProperty("thumbnail")
    private String introThumbnail;

    @JsonProperty("createTime")
    private Timestamp introCreateTime;

    private Integer themeId;

    private String userId;

    @JsonProperty("nickName")
    private String userNickname;

    private Integer rangeId;

    private String rangeName;

    private String themeName;

    private String articleId;

    public FollowIntroVO( String introTitle, String introDescription, Integer introLook, Integer introComment, Integer introLike, Integer introCollect, String introThumbnail, Integer themeId, String userId, String userNickname, Integer rangeId, String rangeName, String themeName, String articleId) {
        this.introTitle = introTitle;
        this.introDescription = introDescription;
        this.introLook = introLook;
        this.introComment = introComment;
        this.introLike = introLike;
        this.introCollect = introCollect;
        this.introThumbnail = introThumbnail;
        this.themeId = themeId;
        this.userId = userId;
        this.userNickname = userNickname;
        this.rangeId = rangeId;
        this.rangeName = rangeName;
        this.themeName = themeName;
        this.articleId = articleId;
    }
}
