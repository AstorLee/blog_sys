package com.example.blog.sys.vo.save;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.sql.Timestamp;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/22 17:32
 **/

@Data
public class UserInfoSaveVO {

    @JsonProperty("nickName")
    private String infoNickname;

    @JsonProperty("description")
    private String infoDescription;

    @JsonProperty("avatar")
    private String infoAvatar;

    @JsonProperty("createTime")
    private Timestamp infoCreateTime;

    private Integer articleNum;

    private Integer cateNum;

    private Integer specialNum;

    private Integer followNum;

    private Integer toFollowNum;

    private Integer collectNum;

}
