package com.example.blog.sys.service;

import com.example.blog.sys.dao.Focus;
import com.example.blog.sys.vo.FocusVO;

import java.util.List;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/2 18:06
 **/

public interface FocusService {

    List<FocusVO> findAllByUserId(String userId);

    List<FocusVO> findAllByTopicId(Integer topicId);

    Boolean saveFocus(Focus focus);

    Boolean delFocus(String focusId);

    Focus findByFocusId(String focusId);
}
