package com.example.blog.sys.dao;

import com.example.blog.sys.enums.FollowTypeEnum;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/30 14:46
 **/

@Entity
@Table(name = "blog_sys_user_follow")
@Data
public class Follow {

    @Id
    @GeneratedValue(generator = "jpa-uuid",strategy = GenerationType.IDENTITY)
    @Column(length = 32)
    private String followId;

    @NotEmpty(message = "来源编号不能为空")
    private String followFrom;

    @NotEmpty(message = "目标编号不能为空")
    private String followTo;

    private String followType = FollowTypeEnum.TOUSER.getType();

}
