package com.example.blog.sys.controller;

import com.example.blog.sys.conf.MyException;
import com.example.blog.sys.dao.Collect;
import com.example.blog.sys.dao.CollectCate;
import com.example.blog.sys.dao.Events;
import com.example.blog.sys.dao.User;
import com.example.blog.sys.enums.ErrorCodeEnum;
import com.example.blog.sys.note.RequireToken;
import com.example.blog.sys.service.CollectCateService;
import com.example.blog.sys.service.CollectService;
import com.example.blog.sys.service.EventsService;
import com.example.blog.sys.service.Impl.RedisTemplateService;
import com.example.blog.sys.vo.ResultVO;
import com.example.blog.sys.vo.save.CollectSaveVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author: LetBeing
 * @Description : 收藏
 * @Date: 2019-08-22 13:00
 **/

@RestController
@RequestMapping("/collect")
@Api(value = "collect",tags = "收藏管理")
public class CollectController {

    @Autowired
    private CollectService collectService;

    @Autowired
    private CollectCateService collectCateService;

    @Autowired
    private RedisTemplateService redisTemplateService;

    @Autowired
    private EventsService eventsService;

    @GetMapping("/{cateId}")
    @RequireToken
    @ApiOperation(value = "按收藏分类编号查询收藏")
    public ResultVO findAllByCateId(@PathVariable("cateId") String cateId) {
        return new ResultVO("成功",1000,collectService.findAllByCateId(cateId));
    }

    @GetMapping("")
    @RequireToken
    @ApiOperation(value = "查询用户所有收藏")
    public ResultVO findAll(HttpServletRequest request) {
        String jwt = request.getHeader("jwt");
        String key = "user_" + jwt;
        User user = redisTemplateService.get(key, User.class);
        if ( user != null ) {
            return new ResultVO("成功",1000,collectService.findAllByCollectFrom(user.getUserId()));
        }else {
            throw new MyException(ErrorCodeEnum.USERERR.getCode(),"用户不存在");
        }
    }

    @PostMapping("")
    @RequireToken
    @ApiOperation(value = "保存收藏")
    public ResultVO saveCollect(@RequestBody CollectSaveVO collectSaveVO,HttpServletRequest request) {
        /**
         * 保存收藏增加收藏夹数目
         * 增加文章收藏数目，增加文章简介收藏数目
         * 创建动态
         * */
        String jwt = request.getHeader("jwt");
        String key = "user_" + jwt;
        User user = redisTemplateService.get(key, User.class);
        if ( user != null ) {
            CollectCate collectCate = collectCateService.findById(collectSaveVO.getCateId());
            if ( collectCate == null ) {
                Collect collect = new Collect();
                BeanUtils.copyProperties(collectSaveVO,collect);
                collect.setCollectFrom(user.getUserId());
                collectService.saveCollect(collect);
                //收藏数 + 1
                collectCate.setCateNum(collectCate.getCateNum() + 1);
                collectCateService.saveCollectCate(collectCate);
                //创建动态
                Events events = new Events();
                events.setEventsFrom(collect.getCollectFrom());
                events.setEventsTo(collect.getCollectToUserId());
                events.setEventsDescription(user.getUserNickname() + "收藏了你的文章");
                eventsService.saveEvents(events);
                return new ResultVO("成功",1000,true);
            }else  {
                throw new MyException(ErrorCodeEnum.OPERATIONERR.getCode(),"参数错误");
            }
        }else {
            throw new MyException(ErrorCodeEnum.USERERR.getCode(),"用户不存在");
        }
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "按收藏编号删除分类")
    @ApiImplicitParam(value = "id",name = "收藏id")
    @RequireToken
    public ResultVO delCollectCate(@PathVariable("id") String id, HttpServletRequest request) {
        String jwt = request.getHeader("jwt");
        String key = "user_" + jwt;
        User user = redisTemplateService.get(key,User.class);
        Collect collect = collectService.findById(id);
        if ( collect != null ) {
            if (collect.getCollectFrom().equals(user.getUserId())) {
                collectService.delCollect(id);
                return new ResultVO("成功",1000,true);
            }else {
                throw new MyException(ErrorCodeEnum.USERERR.getCode(),"非法操作");
            }
        }else {
            throw new MyException(ErrorCodeEnum.USERERR.getCode(),"收藏不存在");
        }
    }

}
