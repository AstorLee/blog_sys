package com.example.blog.sys.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @Author: LetBeing
 * @Description : 专栏VO
 * @Date: 2019/8/27 15:23
 **/

@Data
public class SpecialVO {

    @JsonProperty("id")
    private String specialId;

    @JsonProperty("name")
    private String specialName;

    private Integer articleNum;

    private Integer followNum;

    @JsonProperty("icon")
    private String specialIcon;

    private Integer lookNum;

    private String userId;
}
