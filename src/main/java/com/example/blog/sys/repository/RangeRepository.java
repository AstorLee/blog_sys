package com.example.blog.sys.repository;

import com.example.blog.sys.dao.Range;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/29 11:32
 **/

@Repository
public interface RangeRepository extends JpaRepository<Range,Integer> {

}
