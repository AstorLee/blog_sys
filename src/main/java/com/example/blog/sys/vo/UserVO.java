package com.example.blog.sys.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/22 17:31
 **/

@Data
public class UserVO implements Serializable {

    @JsonProperty("userId")
    private String userId;

    @JsonProperty("account")
    private String userAccount;

    @JsonProperty("email")
    private String userEmail;

    @JsonProperty("phone")
    private Integer userPhone;

    @JsonProperty("nickName")
    private String infoNickname;

    @JsonProperty("description")
    private String infoDescription;

    @JsonProperty("avatar")
    private String infoAvatar;

    private Integer articleNum;

    private Integer cateNum;

    private Integer specialNum;
}
