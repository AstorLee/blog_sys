package com.example.blog.sys.vo.save;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @Author: LetBeing
 * @Description : 文章保存VO
 * @Date: 2019/8/22 18:48
 **/

@Data
public class ArticleSaveVO {

    @JsonProperty("title")
    @NotEmpty(message = "标题不能为空")
    private String articleTitle;

    @JsonProperty("content")
    @NotEmpty(message = "内容不能为空")
    private String articleContent;

    private Integer themeId;

    private String themeName;

    private String specialId;

    private String description;

    private Integer rangeId;

    private String rangeName;

}
