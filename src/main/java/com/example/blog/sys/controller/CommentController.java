package com.example.blog.sys.controller;

import com.example.blog.sys.conf.MyException;
import com.example.blog.sys.dao.*;
import com.example.blog.sys.enums.ErrorCodeEnum;
import com.example.blog.sys.enums.EventsTypeEnum;
import com.example.blog.sys.note.RequireToken;
import com.example.blog.sys.service.*;
import com.example.blog.sys.service.Impl.RedisTemplateService;
import com.example.blog.sys.vo.ResultVO;
import com.example.blog.sys.vo.UserInfoVO;
import com.example.blog.sys.vo.save.CommentSaveVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;

/**
 * @Author: LetBeing
 * @Description : 评论管理
 * @Date: 2019-08-22 11:57
 **/

@RestController
@RequestMapping("/comment")
@Api(value = "comment",tags = "评论管理")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @Autowired
    private RedisTemplateService redisTemplateService;

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private ArticleService articleService;

    @Autowired
    private IntroService introService;

    @Autowired
    private EventsService eventsService;

    @Autowired
    private FocusService focusService;

    @ApiOperation(value = "保存评论")
    @PostMapping("")
    @RequireToken
    public ResultVO saveComment(@RequestBody CommentSaveVO commentSaveVO, HttpServletRequest request) {
        String key = "user_" + request.getHeader("jwt");
        User user = redisTemplateService.get(key,User.class);
        if ( user != null ) {
            UserInfo userInfo = userInfoService.findByUserId(user.getUserId());
            Comment comment = new Comment();
            comment.setCommentFrom(user.getUserId());
            comment.setCommentFromNickname(userInfo.getInfoNickname());
            comment.setCommentFromAvatar(userInfo.getInfoAvatar());
            BeanUtils.copyProperties(commentSaveVO,comment);
            //若评论文章，文章简介评论数 + 1
            if ( "1".equals(commentSaveVO.getCommentType()) ) {
                Article article = articleService.findByArticleId(comment.getArticleId());
                article.setArticleComment(article.getArticleComment() + 1);
                articleService.saveArticle(article);
                Intro intro = introService.findByArticleId(comment.getArticleId());
                intro.setIntroComment(intro.getIntroComment() + 1);
                introService.saveIntro(intro);
                comment.setCommentTo( comment.getArticleId());
            }else if ( "3".equals(commentSaveVO.getCommentType()) ) {
                //看点增加评论数
                Focus focus = focusService.findByFocusId(commentSaveVO.getFocusId());
                focus.setCommentNum( focus.getCommentNum() + 1 );
                focusService.saveFocus(focus);
                comment.setCommentTo( comment.getFocusId());
            }
            //创建动态
            try{
                Events events = new Events();
                events.setEventsFrom(user.getUserId());
                events.setEventsType(EventsTypeEnum.FROMCOMMENT.getType());
                events.setEventsTo(comment.getCommentToUserId());
                if ( "1".equals(comment.getCommentType()) ) {
                    events.setEventsDescription(user.getUserNickname() + "评论了你的文章");
                    events.setEventsTouch(comment.getArticleId());
                }else if ( "2".equals(comment.getCommentType()) ){
                    events.setEventsDescription(user.getUserNickname() + "回复了你的评论");
                    events.setEventsTouch(comment.getCommentSid());
                } else if ( "3".equals(comment.getCommentType()) ){
                    events.setEventsDescription(user.getUserNickname() + "评论了你的看点");
                    events.setEventsTouch(comment.getFocusId());
                }
                eventsService.saveEvents(events);
            } catch ( Exception e ) {
                e.printStackTrace();
            }
            return new ResultVO("成功",1000,commentService.saveComment(comment));
        }else {
            throw new MyException(ErrorCodeEnum.USERERR.getCode(),"非法的操作");
        }
    }

    @ApiOperation(value = "删除评论")
    @DeleteMapping("/{id}")
    @RequireToken
    public ResultVO delComment(@PathVariable("id") String id) {
        Comment comment = commentService.findByCommentId(id);
        Article article = articleService.findByArticleId(comment.getArticleId());
        article.setArticleComment(article.getArticleComment() - 1);
        articleService.saveArticle(article);
        Intro intro = introService.findByArticleId(comment.getArticleId());
        intro.setIntroComment(intro.getIntroComment() - 1);
        introService.saveIntro(intro);
        return new ResultVO("成功",1000,commentService.delComment(id));
    }

    @ApiOperation(value = "根据文章编号或上级评论编号获得评论列表")
    @GetMapping("")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id",value = "编号",paramType = "query"),
            @ApiImplicitParam(name = "type" ,value = "类型",paramType = "query",required = true,defaultValue = "1")
    })
    public ResultVO findAllById(@RequestParam("id") String id,@RequestParam("type") String type) {
        return new ResultVO("成功",1000,commentService.findAllByCommentAndType(id,type));
    }

}
