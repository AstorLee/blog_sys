package com.example.blog.sys.service;

import com.example.blog.sys.dao.Events;
import com.example.blog.sys.vo.EventsVO;

import java.util.List;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/6 15:02
 **/

public interface EventsService {

    boolean saveAll(List<Events> eventsList);

    boolean delAll(String eventsTo);

    boolean saveEvents(Events events);

    List<EventsVO> findAllEventsVO(String eventsTo);

    List<Events> findAllEvents(String eventsTo);

    Events findByEventsId(String eventsId);

}
