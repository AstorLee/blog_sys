package com.example.blog.sys.controller;

import com.example.blog.sys.conf.MyException;
import com.example.blog.sys.dao.Special;
import com.example.blog.sys.dao.User;
import com.example.blog.sys.enums.ErrorCodeEnum;
import com.example.blog.sys.note.RequireToken;
import com.example.blog.sys.service.Impl.RedisTemplateService;
import com.example.blog.sys.service.SpecialService;
import com.example.blog.sys.vo.ResultVO;
import com.example.blog.sys.vo.SpecialVO;
import com.example.blog.sys.vo.save.SpecialSaveVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @Author: LetBeing
 * @Description : 用户专栏
 * @Date: 2019/9/2 11:01
 **/

@RestController
@Api(tags = "专栏管理" ,value = "special")
@RequestMapping("/special")
public class SpecialController {

    @Autowired
    private SpecialService specialService;

    @Autowired
    private RedisTemplateService redisTemplateService;

    @GetMapping("")
    @ApiOperation(value = "根据用户编号查询用户专栏")
    @RequireToken
    public ResultVO findByUserId(HttpServletRequest request){
        String jwt = request.getHeader("jwt");
        if ( jwt == null ) {
            throw new MyException(ErrorCodeEnum.USERERR.getCode(),"用户未登录");
        }else {
            String userJwt = "user_" + jwt;
            User user =  redisTemplateService.get(userJwt,User.class);
            if (user == null) {
                throw new MyException(ErrorCodeEnum.USERERR.getCode(),"jwt错误");
            }else {
                List<SpecialVO> specialVOList = specialService.findAllByUserId(user.getUserId());
                return new ResultVO("成功",1000,specialVOList);
            }
        }
    }

    @PostMapping("")
    @ApiOperation(value = "保存专栏")
    @Transactional(rollbackFor = Exception.class)
    public ResultVO saveSpecial(@RequestBody SpecialSaveVO specialSaveVO,HttpServletResponse response) {
        String jwt = response.getHeader("jwt");
        if ( jwt == null ) {
            throw new MyException(ErrorCodeEnum.USERERR.getCode(),"jwt不存在");
        }else {
            String userJwt = "user" + jwt;
            User user =  redisTemplateService.get(userJwt,User.class);
            if (user == null) {
                throw new MyException(ErrorCodeEnum.USERERR.getCode(),"jwt错误");
            }else {
                Special special = new Special();
                BeanUtils.copyProperties(specialSaveVO,special);
                special.setUserId(user.getUserId());
                Boolean status = specialService.saveSpecial(special);
                if (status) {
                    return new ResultVO("成功",1000,status);
                }else {
                    throw new MyException(ErrorCodeEnum.OPERATIONERR.getCode(),"系统错误");
                }
            }
        }
    }


    @DeleteMapping("/{id}")
    @ApiOperation(value = "按专栏id删除专栏")
    @Transactional(rollbackFor = Exception.class)
    public ResultVO delSpecial(@PathVariable("id") String id,HttpServletResponse response) {
        String jwt = response.getHeader("jwt");
        if ( jwt == null ) {
            throw new MyException(ErrorCodeEnum.USERERR.getCode(),"jwt不存在");
        }else {
            String userJwt = "user" + jwt;
            User user =  redisTemplateService.get(userJwt,User.class);
            if (user == null) {
                throw new MyException(ErrorCodeEnum.USERERR.getCode(),"jwt错误");
            }else {
                Special special = specialService.findBySpecialId(id);
                if ( special.getUserId().equals(user.getUserId()) ) {
                    Boolean status = specialService.delSpecial(id);
                    return new ResultVO("成功",1000,status);
                }else {
                    throw new MyException(ErrorCodeEnum.USERERR.getCode(),"非法的操作");
                }
            }
        }
    }

}
