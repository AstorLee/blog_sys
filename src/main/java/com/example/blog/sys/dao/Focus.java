package com.example.blog.sys.dao;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.sql.Timestamp;

/**
 * @Author: LetBeing
 * @Description : 看点
 * @Date: 2019/9/2 17:52
 **/

@Data
@Entity
@Table(name = "blog_sys_focus")
@GenericGenerator(name = "jpa-uuid",strategy = "uuid")
public class Focus {

    @Id
    @GeneratedValue(generator = "jpa-uuid",strategy = GenerationType.IDENTITY)
    @Column(length = 32)
    private String focusId;

    @NotEmpty(message = "内容不能为空")
    private String focusContent;

    private String topicName;

    private Integer topicId;

    @NotEmpty(message = "用户编号不能为空")
    private String userId;

    private String userAvatar;

    private Timestamp focusCreateTime = new Timestamp(System.currentTimeMillis());

    private String userNickname;

    private Integer commentNum = 0;

    private Integer likeNum = 0;

    private String focusImgs;
}
