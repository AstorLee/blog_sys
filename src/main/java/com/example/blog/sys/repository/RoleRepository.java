package com.example.blog.sys.repository;

import com.example.blog.sys.dao.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/3 10:14
 **/

@Repository
public interface RoleRepository extends JpaRepository<Role,Integer> {

    Role findByRoleId(Integer roleId);
}
