package com.example.blog.sys.repository;

import com.example.blog.sys.dao.Ranking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @Author: LetBeing
 * @Description : 待定
 * @Date: 2019/9/11 14:43
 **/

@Repository
public interface RankingRepository extends JpaRepository<Ranking,String> {

}
