package com.example.blog.sys.dao;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/2 14:35
 **/

@Entity
@Data
@Table(name = "blog_sys_user_collect")
public class Collect {

    @Id
    @GeneratedValue(generator = "jpa-uuid",strategy = GenerationType.IDENTITY)
    @Column(length = 32)
    private String collectId;

    @NotEmpty(message = "来源编号不能为空")
    private String collectFrom;

    @NotEmpty(message = "目标编号不能为空")
    private String collectTo;

    @NotEmpty(message = "目标用户编号不能为空")
    private String collectToUserId;

    @NotEmpty(message = "分类编号不能为空")
    private String cateId;

}
