package com.example.blog.sys.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/2 18:36
 **/

@Data
public class TopicVO {

    @JsonProperty("id")
    private Integer topicId;

    @JsonProperty("name")
    private String topicName;

    @JsonProperty("icon")
    private String topicIcon;

    private Integer followNum;

    private Integer focusNum;
}
