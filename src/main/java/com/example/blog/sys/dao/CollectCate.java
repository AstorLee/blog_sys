package com.example.blog.sys.dao;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/2 14:47
 **/

@Entity
@Data
@Table(name = "blog_sys_user_collect_cate")
public class CollectCate {

    @Id
    @GeneratedValue(generator = "jpa-uuid",strategy = GenerationType.IDENTITY)
    @Column(length = 32)
    private String cateId;

    @NotEmpty(message = "分类名称不能为空")
    private String cateName;

    private Integer cateNum = 0;

    @NotEmpty(message = "用户编号不能为空")
    private String userId;

    private String cateIcon;

}
