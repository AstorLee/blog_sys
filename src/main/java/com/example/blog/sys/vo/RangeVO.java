package com.example.blog.sys.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/29 11:33
 **/

@Data
public class RangeVO {

    @JsonProperty("id")
    private Integer rangeId;

    @JsonProperty("name")
    private String rangeName;
}
