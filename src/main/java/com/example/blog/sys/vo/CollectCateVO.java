package com.example.blog.sys.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/2 14:55
 **/

@Data
public class CollectCateVO {

    @JsonProperty("id")
    private String cateId;

    @JsonProperty("name")
    private String cateName;

    @JsonProperty("num")
    private Integer cateNum;

    private String userId;

    @JsonProperty("icon")
    private String cateIcon;
}
