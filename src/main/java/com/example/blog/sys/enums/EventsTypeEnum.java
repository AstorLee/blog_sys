package com.example.blog.sys.enums;

import lombok.Getter;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/6 14:55
 **/

@Getter
public enum EventsTypeEnum {
    /**
     *
     * */

    FROMFOLLOW("1","关注人的更新了"),
    FROMFOCUS("2","被关注"),
    FROMLIKE("3","被点赞"),
    FROMCOLLECT("4","被收藏"),
    FROMCOMMENT("5","被评论");

    private String type;

    private String msg;

    EventsTypeEnum(String type,String msg) {
        this.type = type;
        this.msg = msg;
    }
}
