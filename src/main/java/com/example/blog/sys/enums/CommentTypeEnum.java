package com.example.blog.sys.enums;

import lombok.Getter;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/11 15:12
 **/

@Getter
public enum CommentTypeEnum {

    /**
     * */

    TOARTICLE("1","对文章"),TOCOMMENT("2","对评论");

    private String type;

    private String msg;

    CommentTypeEnum(String type, String msg) {
        this.type = type;
        this.msg = msg;
    }

}
