package com.example.blog.sys.vo.save;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/2 16:07
 **/

@Data
public class CollectSaveVO {

    @JsonProperty("toId")
    private String collectTo;

    private String cateId;
}
