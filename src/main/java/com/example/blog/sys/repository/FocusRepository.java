package com.example.blog.sys.repository;

import com.example.blog.sys.dao.Focus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/2 18:05
 **/

@Repository
public interface FocusRepository extends JpaRepository<Focus,String> {

    List<Focus> findAllByTopicId(Integer topicId);

    List<Focus> findAllByUserId(String userId);

    Focus findByFocusId(String id);
}
