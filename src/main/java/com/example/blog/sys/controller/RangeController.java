package com.example.blog.sys.controller;

import com.example.blog.sys.service.Impl.RangeServiceImpl;
import com.example.blog.sys.vo.RangeVO;
import com.example.blog.sys.vo.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/29 11:43
 **/

@RestController
@RequestMapping("/range")
@Api(value = "range",tags = "范围管理")
public class RangeController {

    @Autowired
    RangeServiceImpl rangeService;


    @ApiOperation(value = "查询所有范围")
    @GetMapping("")
    public ResultVO findAll() {
        List<RangeVO> rangeVOList = rangeService.findAll();
        ResultVO resultVO = new ResultVO("成功",1000,rangeVOList);
        return resultVO;
    }

}
