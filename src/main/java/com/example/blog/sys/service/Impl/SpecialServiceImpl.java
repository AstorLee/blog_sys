package com.example.blog.sys.service.Impl;

import com.example.blog.sys.dao.Special;
import com.example.blog.sys.repository.SpecialRepository;
import com.example.blog.sys.service.SpecialService;
import com.example.blog.sys.vo.SpecialVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/27 17:09
 **/

@Service
public class SpecialServiceImpl implements SpecialService {

    @Autowired
    SpecialRepository repository;


    @Override
    public Boolean saveSpecial(Special special) {
        try {
            repository.save(special);
        } catch (RuntimeException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public Boolean delSpecial(String specialId) {
        try {
            repository.deleteBySpecialId(specialId);
        }catch (RuntimeException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public List<SpecialVO> findAllByUserId(String userId) {
        List<Special> specialList =  repository.findAllByUserId(userId);
        List<SpecialVO> specialVOList = new ArrayList<>();
        for (Special special:specialList){
            SpecialVO specialVO = new SpecialVO();
            BeanUtils.copyProperties(special,specialVO);
            specialVOList.add(specialVO);
        }
        return specialVOList;
    }

    @Override
    public Special findBySpecialId(String specialId) {
        return repository.findBySpecialId(specialId);
    }
}
