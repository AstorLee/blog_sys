package com.example.blog.sys.dao;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/29 11:28
 **/

@Data
@Table(name = "blog_sys_range")
@Entity
public class Range {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer rangeId;

    @NotEmpty(message = "范围名不能为空")
    private String rangeName;
}
