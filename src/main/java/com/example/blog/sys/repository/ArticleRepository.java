package com.example.blog.sys.repository;

import com.example.blog.sys.dao.Article;
import com.example.blog.sys.vo.IntroVO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019-08-22 11:40
 **/

@Repository
public interface ArticleRepository extends JpaRepository<Article,String> {

    Article findByArticleId(String articleId);
}
