package com.example.blog.sys.enums;

import lombok.Getter;

/**
 * @Author: LetBeing
 * @Description : 用户账号类型
 * @Date: 2019/8/26 11:09
 **/

@Getter
public enum UserStatusEnum {

    /**
     *@START 启用|或激活状态
     * @DISABLED 禁用|或未激活状态
     * */

    START("1"),DISABLED("0");

    private final String status;

    UserStatusEnum(String status) {
        this.status = status;
    }
}
