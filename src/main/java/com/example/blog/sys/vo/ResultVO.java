package com.example.blog.sys.vo;


import lombok.Data;

/**
 * @Author: LetBeing
 * @Description : 同一返回类
 * @code 1000==>通过 1001==> 失败 1002==>失败 1003==>没有权限 1004==>不存在方法
 * @Date: 2019-08-22 10:52
 **/
@Data
public class ResultVO <T> {

    private String message;

    private Integer code;

    private T data;

    public ResultVO(String message, Integer code, T data) {
        this.message = message;
        this.code = code;
        this.data = data;
    }

    public ResultVO(String message, Integer code) {
        this.message = message;
        this.code = code;
    }

    public ResultVO() {
    }

}
