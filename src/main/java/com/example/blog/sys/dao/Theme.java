package com.example.blog.sys.dao;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/27 15:06
 **/

@Entity
@Data
@Table(name = "blog_sys_theme")
public class Theme {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer themeId;

    @NotEmpty(message = "主题名不能为空")
    @NotNull(message = "主题名不能为空")
    private String themeName;

    private Integer themeNum = 0;

    @NotEmpty(message = "范围编号不能为空")
    private Integer rangeId;

}
