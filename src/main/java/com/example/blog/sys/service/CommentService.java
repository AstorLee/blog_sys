package com.example.blog.sys.service;

import com.example.blog.sys.dao.Comment;
import com.example.blog.sys.vo.CommentVO;

import java.util.List;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/11 15:37
 **/

public interface CommentService {

    /**
     * @param toId 可能是文章编号也可能是评论编号
     * @return 返回评论列表
     * */
    List<CommentVO> findAllByCommentAndType(String toId,String type);

    boolean saveComment(Comment comment);

    boolean delComment(String commentId);

    Comment findByCommentId(String commentId);

}
