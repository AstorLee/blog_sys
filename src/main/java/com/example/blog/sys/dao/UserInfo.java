package com.example.blog.sys.dao;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.sql.Timestamp;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019-08-22 16:34
 **/

@Entity
@Table(name = "blog_sys_user_info")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@Data
public class UserInfo {

    @Id
    @GeneratedValue(generator = "jpa-uuid",strategy = GenerationType.IDENTITY)
    @Column(length = 32)
    private String infoId;

    private String infoNickname = "无名";

    private String infoDescription = "这个人很懒，什么都没留下";

    private String infoAvatar = "https://www.baidu.com/s?rsv_idx=1&wd=c%2B%2B&ie=utf-8&rsv_cq=springboot+RequestParam%E8%AE%BE%E7%BD%AE%E9%9D%9E%E5%BF%85%E5%A1%AB&rsv_dl=0_right_recommends_merge_28335&euri=824";

    @NotEmpty(message = "用户编号不能为空")
    private String userId;

    private Timestamp infoCreateTime;

    private Integer articleNum = 0;

    private Integer cateNum = 0;

    private Integer specialNum = 0;

    private Integer followNum = 0;

    private Integer toFollowNum = 0;

    private Integer collectNum = 0;

}
