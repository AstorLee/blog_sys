package com.example.blog.sys.conf;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: LetBeing
 * @Description : 捕获全局异常
 * @Date: 2019/8/27 17:49
 **/

@ControllerAdvice(basePackages = "com.example.blog.sys.controller")
public class AdviceError {

    @ResponseBody
    @ExceptionHandler(value = MyException.class)
    public Map<String,Object> exceptionHandler(MyException ex) {
        Map<String,Object> map  = new HashMap<String,Object>();
        map.put("code",ex.getErrorCode());
        map.put("message",ex.getErrorMsg());
        map.put("data",true);
        //这里可以加上我们其他的异常处理代码，比如日志记录，，，
        return map;
    }
}
