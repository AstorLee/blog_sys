package com.example.blog.sys.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @Author: LetBeing
 * @Description : 收藏VO
 * @Date: 2019/9/2 14:56
 **/

@Data
public class CollectVO {

    @JsonProperty("id")
    private String collectId;

    @JsonProperty("toId")
    private String collectTo;

    private String cateId;
}
