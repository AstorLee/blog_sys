package com.example.blog.sys.service.Impl;

import com.example.blog.sys.dao.Comment;
import com.example.blog.sys.repository.CommentRepository;
import com.example.blog.sys.service.CommentService;
import com.example.blog.sys.vo.CommentVO;
import com.example.blog.sys.vo.save.CommentSaveVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/11 16:17
 **/

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentRepository repository;

    @Override
    public List<CommentVO> findAllByCommentAndType(String toId,String type) {
        List<Comment> commentList;
        if ( "1".equals(type) ) {
            commentList = repository.findAllByArticleId(toId);
        }else {
            commentList = repository.findAllByFocusId(toId);
        }
        List<CommentVO> commentVOList = new ArrayList<CommentVO>();
        for (Comment comment : commentList) {
            CommentVO commentVO = new CommentVO();
            BeanUtils.copyProperties(comment, commentVO);
            if (comment.getCommentSid() == null) {
                commentVOList.add(this.loopFindSid(commentVO, commentList));
            }
        }
        return commentVOList;
    }

    private CommentVO loopFindSid(CommentVO commentVO, List<Comment> commentList) {
        for (Comment comment : commentList) {
            if (commentVO.getCommentId().equals(comment.getCommentSid())) {
                CommentVO machComment = new CommentVO();
                BeanUtils.copyProperties(comment, machComment);
                commentVO.getReplyList().add(this.loopFindSid(machComment, commentList));
            }
        }
        return commentVO;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean saveComment(Comment comment) {
        try {
            repository.save(comment);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public Comment findByCommentId(String commentId) {
        return repository.findByCommentId(commentId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean delComment(String commentId) {
        try {
            repository.deleteById(commentId);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
