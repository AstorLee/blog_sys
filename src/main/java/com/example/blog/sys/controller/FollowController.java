package com.example.blog.sys.controller;

import com.example.blog.sys.conf.MyException;
import com.example.blog.sys.dao.*;
import com.example.blog.sys.enums.ErrorCodeEnum;
import com.example.blog.sys.enums.EventsTypeEnum;
import com.example.blog.sys.note.RequireToken;
import com.example.blog.sys.service.EventsService;
import com.example.blog.sys.service.FollowService;
import com.example.blog.sys.service.Impl.TopicServiceImpl;
import com.example.blog.sys.service.Impl.RedisTemplateService;
import com.example.blog.sys.service.UserInfoService;
import com.example.blog.sys.vo.FollowVO;
import com.example.blog.sys.vo.ResultVO;
import com.example.blog.sys.vo.TopicVO;
import com.example.blog.sys.vo.UserInfoVO;
import com.example.blog.sys.vo.save.FollowSaveVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: LetBeing
 * @Description : 粉丝管理
 * @Date: 2019-08-22 13:04
 **/

@RestController
@RequestMapping("/follow")
@Api(value = "follow",tags = "粉丝管理")
public class FollowController {

    @Autowired
    private FollowService followService;

    @Autowired
    private RedisTemplateService redisTemplateService;

    @Autowired
    private TopicServiceImpl topicService;

    @Autowired
    private EventsService eventsService;

    @GetMapping("/{type}")
    @ApiOperation(value = "按关注分类查询关注")
    @RequireToken
    public ResultVO findAllByFollowType(@PathVariable("type") String type,HttpServletRequest request) {
        String key = "user_" + request.getHeader("jwt");
        User user = redisTemplateService.get(key,User.class);
        String userId = user.getUserId();
        List<FollowVO> followVOList = followService.findAllByFollowTypeAndFollowFrom(type,userId);

        if ( "4".equals(type) ) {
            List<TopicVO> topicVOList = topicService.findAll();
            List<FollowVO> newFollowVOList = new ArrayList<FollowVO>();
            for (FollowVO followVO:followVOList) {
                for (TopicVO topicVO:topicVOList) {
                    if ( followVO.getFollowTo().equals(String.valueOf(topicVO.getTopicId())) ) {
//                        followVO.setData(topicVO);
                        newFollowVOList.add(followVO);
                    }
                }
            }
            return new ResultVO("成功",1000,newFollowVOList);
        }
        return new ResultVO("成功",1000,followVOList);
    }

    @PostMapping("")
    @ApiOperation(value = "新增关注")
    @RequireToken
    public ResultVO saveFollow(@RequestBody FollowSaveVO followSaveVO, HttpServletRequest request) {

        /**
         * @Description 更新
         * */

        String key = "user_" + request.getHeader("jwt");
        User user = redisTemplateService.get(key,User.class);
        if ( user != null ) {
            Follow follow = new Follow();
            BeanUtils.copyProperties(followSaveVO,follow);
            if ( "4".equals(follow.getFollowType()) ) {
                Topic topic = topicService.findById(Integer.valueOf(follow.getFollowTo()));
                topic.setFollowNum(topic.getFollowNum() + 1);
                topicService.saveTopic(topic);
            }
            follow.setFollowFrom(user.getUserId());
            boolean status = followService.saveFollow(follow);
            if ( "1".equals(follow.getFollowType()) ) {
                Events events = new Events();
                events.setEventsType(EventsTypeEnum.FROMFOCUS.getType());
                events.setEventsFrom(user.getUserId());
                events.setEventsTo(followSaveVO.getFollowTo());
                events.setEventsDescription(user.getUserNickname() +"关注了你");
                eventsService.saveEvents(events);
            }
            return new ResultVO("成功",1000,status);
        }else {
            throw  new MyException(ErrorCodeEnum.USERERR.getCode(),"用户不存在");
        }
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除关注")
    @RequireToken
    public ResultVO delFollow(@PathVariable("id") String id,HttpServletRequest request) {
        String key = "user_" + request.getHeader("jwt");
        User user = redisTemplateService.get(key,User.class);
        if ( user != null ) {
            Follow follow = followService.findByFollowId(id);
            if ( user.getUserId().equals(follow.getFollowFrom()) ) {
//                Topic topic = topicService.findById(Integer.valueOf(follow.getFollowTo()));
//                topic.setFollowNum(topic.getFollowNum() - 1);
//                topicService.saveTopic(topic);
                boolean status = followService.delFollow(id);
                return new ResultVO("成功",1000,status);
            }else {
                throw  new MyException(ErrorCodeEnum.USERERR.getCode(),"非法操作");
            }
        }else {
            throw  new MyException(ErrorCodeEnum.USERERR.getCode(),"用户不存在");
        }
    }
}
