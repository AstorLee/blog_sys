package com.example.blog.sys.controller;

import com.example.blog.sys.conf.MyException;
import com.example.blog.sys.dao.Events;
import com.example.blog.sys.dao.User;
import com.example.blog.sys.enums.ErrorCodeEnum;
import com.example.blog.sys.enums.EventStatusEnum;
import com.example.blog.sys.note.RequireToken;
import com.example.blog.sys.service.EventsService;
import com.example.blog.sys.service.Impl.RedisTemplateService;
import com.example.blog.sys.vo.EventsVO;
import com.example.blog.sys.vo.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.swing.plaf.PanelUI;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: LetBeing
 * @Description : 动态Controller
 * @Date: 2019-08-22 13:03
 **/

@RestController
@RequestMapping("/events")
@Api(value = "动态管理",tags = "动态管理")
public class EventsController {

    @Autowired
    private EventsService eventsService;

    @Autowired
    private RedisTemplateService redisTemplateService;

    @GetMapping("/{id}")
    @ApiOperation(value = "根据用户编号获得用户动态")
    public ResultVO findAllByUserId(@PathVariable("id") String id) {
        if ( id != null ) {
            return new ResultVO("成功",1000,eventsService.findAllEvents(id));
        }else {
            throw new MyException(ErrorCodeEnum.USERERR.getCode(),"参数为空");
        }
    }

    @GetMapping("")
    @ApiOperation(value = "获得用户所有动态")
    @RequireToken
    public ResultVO findAllByUser(HttpServletRequest request) {

        String key = "user_" + request.getHeader("jwt");
        System.out.println(key);
        User user = redisTemplateService.get(key, User.class);

        if ( user != null ) {
            List<Events> eventsList = eventsService.findAllEvents(user.getUserId());
            List<EventsVO> eventsVOList = new ArrayList<>();
            for ( Events events:eventsList ) {
                EventsVO eventsVO = new EventsVO();
                BeanUtils.copyProperties(events,eventsVO);
                eventsVOList.add(eventsVO);
            }
            return new ResultVO("成功",1000,eventsVOList);
        }else {
            throw new MyException(ErrorCodeEnum.USERERR.getCode(),"未登录或者不存在");
        }
    }

    @DeleteMapping("")
    @ApiOperation(value = "删除用户所有动态")
    @RequireToken
    public ResultVO delAllByUserId(HttpServletRequest request) {
        String key = "user_" + request.getHeader("jwt");
        User user = redisTemplateService.get(key,User.class);
        if ( user != null ) {
            boolean status = eventsService.delAll(user.getUserId());
            return new ResultVO("成功",1000,status);
        }else {
            throw new MyException(ErrorCodeEnum.USERERR.getCode(),"非法操作");
        }
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "根据用户编号设置动态全部已读")
    @RequireToken
    public ResultVO updateAllByUserId(@PathVariable("id") String id, HttpServletRequest request) {
        /**
         * 若存在id则设置单条否则全部设置
         * */

        if ( id != null) {
            Events events = eventsService.findByEventsId(id);
            events.setEventsStatus(EventStatusEnum.READ.getStatus());
            boolean status = eventsService.saveEvents(events);
            return new ResultVO("成功",1000,status);
        }else {
            String key = "user_" + request.getHeader("jwt");
            User user = redisTemplateService.get(key,User.class);
            if ( user != null ) {
                List<Events> eventsList = eventsService.findAllEvents(user.getUserId());
                List<Events> newEventsList = new ArrayList<Events>();
                for( Events events:eventsList ) {
                    events.setEventsStatus(EventStatusEnum.READ.getStatus());
                    newEventsList.add(events);
                }
                boolean status = eventsService.saveAll(newEventsList);
                return new ResultVO("成功",1000,status);
            }else {
                throw new MyException(ErrorCodeEnum.USERERR.getCode(),"非法操作");
            }
        }
    }

}
