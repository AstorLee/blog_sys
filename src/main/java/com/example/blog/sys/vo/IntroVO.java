package com.example.blog.sys.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/22 18:41
 **/

@Data
public class IntroVO implements Serializable {

    @JsonProperty("title")
    private String introTitle;

    @JsonProperty("description")
    private String introDescription;

    @JsonProperty("look")
    private Integer introLook;

    @JsonProperty("comment")
    private Integer introComment;

    @JsonProperty("like")
    private Integer introLike;

    @JsonProperty("collect")
    private Integer introCollect;

    @JsonProperty("thumbnail")
    private String introThumbnail;

    @JsonProperty("createTime")
    private Timestamp introCreateTime;

    private Integer themeId;

    private String userId;

    @JsonProperty("avatar")
    private String userAvatar;

    @JsonProperty("nickName")
    private String userNickname;

    private Integer rangeId;

    private String rangeName;

    private String themeName;

    private String articleId;

}
