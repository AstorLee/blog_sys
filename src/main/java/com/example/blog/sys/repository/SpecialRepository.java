package com.example.blog.sys.repository;

import com.example.blog.sys.dao.Special;
import com.example.blog.sys.vo.SpecialVO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/27 17:04
 **/

@Repository
public interface SpecialRepository extends JpaRepository<Special,String> {

    List<Special> findAllByUserId(String id);

    Boolean deleteBySpecialId(String specialId);

    Special findBySpecialId(String id);

}
