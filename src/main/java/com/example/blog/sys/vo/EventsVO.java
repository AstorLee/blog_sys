package com.example.blog.sys.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.sql.Timestamp;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/6 15:07
 **/

@Data
public class EventsVO {

    @JsonProperty("id")
    private String eventsId;

    @JsonProperty("type")
    private String eventsType;

    @JsonProperty("from")
    private String eventsFrom;

    @JsonProperty("to")
    private String eventsTo;

    @JsonProperty("status")
    private String eventsStatus;

    @JsonProperty("description")
    private String eventsDescription;

    @JsonProperty("touch")
    private String eventsTouch;

    @JsonProperty("createTime")
    private Timestamp eventsCreateTime;

}
