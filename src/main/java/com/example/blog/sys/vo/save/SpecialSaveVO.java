package com.example.blog.sys.vo.save;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/2 11:17
 **/

@Data
public class SpecialSaveVO {

    @JsonProperty("name")
    private String specialName;

    @JsonProperty("icon")
    private String specialIcon;

}
