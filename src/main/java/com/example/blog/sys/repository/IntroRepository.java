package com.example.blog.sys.repository;

import com.example.blog.sys.dao.Intro;
import com.example.blog.sys.vo.AuthorVO;
import com.example.blog.sys.vo.FollowIntroVO;
import com.example.blog.sys.vo.IntroVO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author: LetBeing
 * @Description : 文章简介dto
 * @Date: 2019/8/22 18:45
 **/

@Repository
public interface IntroRepository extends JpaRepository<Intro,String> {

    Boolean deleteByArticleId(String articleId);

    Page<Intro> findAllByThemeId(Integer themeId, Pageable pageable);

    List<Intro> findAllByUserId(String userId);

    Intro findByIntroId(String introId);

    Intro findByArticleId(String articleId);

    Page<Intro> findAllByRangeId(Integer rangeId, Pageable pageable);

    @Query("select new com.example.blog.sys.vo.FollowIntroVO( a.introTitle,a.introDescription, a.introLook, a.introComment, a.introLike, a.introCollect,a.introThumbnail, a.themeId,a.userId,a.userNickname, a.rangeId,a.rangeName,a.themeName,a.articleId ) from Intro a left join Follow b on b.followTo = a.userId where b.followFrom = ?1")
    List<FollowIntroVO> findAllByFollowFromId(String userId);

    /**
     * 根据文章点赞数排序并去重作者榜
     * */
    @Query("select new com.example.blog.sys.vo.AuthorVO(a.userId, a.userAvatar, a.userNickname) from Intro a order by a.introLike ASC ")
    List<AuthorVO> findAuthorList();

//    Page<Intro> findAllByOrOrderByIntroLike(Pageable pageable);
}
