package com.example.blog.sys.service.Impl;

import com.example.blog.sys.dao.Topic;
import com.example.blog.sys.repository.TopicRepository;
import com.example.blog.sys.vo.TopicVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/2 18:06
 **/

@Service
public class TopicServiceImpl {

    @Autowired
    private TopicRepository repository;

    public List<TopicVO> findAll() {
        List<Topic> topicList = repository.findAll();
        List<TopicVO> topicVOList = new ArrayList<TopicVO>();
        for (Topic topic:topicList) {
            TopicVO topicVO = new TopicVO();
            BeanUtils.copyProperties(topic,topicVO);
            topicVOList.add(topicVO);
        }
        return topicVOList;
    }

    @Transactional(rollbackFor = Exception.class)
    public Boolean delTopicByTopicId(Integer id) {
        try {
            repository.deleteById(id);
        }catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Transactional(rollbackFor = Exception.class)
    public Boolean saveTopic(Topic topic) {
        try {
            repository.save(topic);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public Topic findById(Integer topicId){
        return repository.findByTopicId(topicId);
    }

}
