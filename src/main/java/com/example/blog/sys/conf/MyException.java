package com.example.blog.sys.conf;

import com.example.blog.sys.enums.ErrorCodeEnum;
import lombok.Data;

/**
 * @Author: LetBeing
 * @Description : 自定义异常类
 * @Date: 2019/8/27 17:54
 **/

@Data
public class MyException extends RuntimeException {

    private Integer errorCode = ErrorCodeEnum.UNKNOWNERR.getCode();

    private String errorMsg;

    private Boolean errorStatus = false;

    public MyException(Integer errorCode, String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public MyException(Integer errorCode, String errorMsg, Boolean errorStatus) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
        this.errorStatus = errorStatus;
    }
}
