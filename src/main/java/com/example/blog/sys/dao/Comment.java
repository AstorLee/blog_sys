package com.example.blog.sys.dao;

import com.example.blog.sys.enums.CommentTypeEnum;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.sql.Timestamp;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/11 15:08
 **/

@Data
@Entity
@Table(name = "blog_sys_comment")
@GenericGenerator(name = "jpa-uuid",strategy = "uuid")
public class Comment {

    @Id
    @GeneratedValue(generator = "jpa-uuid",strategy = GenerationType.IDENTITY)
    @Column(length = 32)
    private String commentId;

    private String commentContent;

    private String commentFrom;

    private String commentFromNickname;

    private String commentFromAvatar;

    @NotEmpty(message = "目标用户编号不能为空")
    private String commentToUserId;

    private String commentTo;

    private String commentType = CommentTypeEnum.TOARTICLE.getType();

    private Timestamp commentCreateTime = new Timestamp(System.currentTimeMillis());

    private String commentSid;

    private String articleId;

    private String focusId;

    private String commentToNickname;
}
