package com.example.blog.sys.repository;

import com.example.blog.sys.dao.Points;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/22 17:43
 **/

@Repository
public interface PointsRepository extends JpaRepository<Points,Integer> {

}
