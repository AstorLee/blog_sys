package com.example.blog.sys.vo.save;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * @Author: LetBeing
 * @Description : focusSaveVO
 * @Date: 2019/9/2 18:09
 **/

@Data
public class FocusSaveVO {

    @JsonProperty("content")
    private String focusContent;

    private String topicName;

    private Integer topicId;

    private List<String> imgList;

}
