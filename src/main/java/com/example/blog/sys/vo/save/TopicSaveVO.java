package com.example.blog.sys.vo.save;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/2 18:33
 **/

@Data
public class TopicSaveVO {

    @JsonProperty("name")
    private String topicName;

    @JsonProperty("icon")
    private String topicIcon;
}
