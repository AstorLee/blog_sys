package com.example.blog.sys.enums;

import lombok.Getter;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/29 10:29
 **/

@Getter
public enum LikeTypeEnum {

    /**
     * @ARTICLE 对文章
     * @COMMENT 对评论
     * */

    ARTICLE("0","文章"),
    COMMENT("1","评论");

    private final String type;

    private String name;

    LikeTypeEnum(String type,String name) {
        this.type = type;
        this.name = name;
    }
}
