package com.example.blog.sys.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/2 18:08
 **/

@Data
public class FocusVO {

    @JsonProperty("id")
    private String focusId;

    @JsonProperty("content")
    private String focusContent;

    private String topicName;

    private Integer topicId;

    private String userId;

    @JsonProperty("avatar")
    private String userAvatar;

    @JsonProperty("nickName")
    private String userNickname;

    @JsonProperty("createTime")
    private Timestamp focusCreateTime;

    private Integer commentNum;

    private Integer likeNum;

    private List<CommentVO> commentVOList;

    private String[] imgList;
}
