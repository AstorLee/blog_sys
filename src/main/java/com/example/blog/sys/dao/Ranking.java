package com.example.blog.sys.dao;

import com.example.blog.sys.enums.RankingTypeEnum;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/11 14:34
 **/


@Data
@Entity
@Table(name = "blog_sys_ranking")
@GenericGenerator(name = "jpa-uuid",strategy = "uuid")
public class Ranking {

    @Id
    @GeneratedValue(generator = "jpa-uuid",strategy = GenerationType.IDENTITY)
    @Column(length = 32)
    private String rankingId;

    private String rankingType = RankingTypeEnum.HOT.getType();

    private String userId;

    private Integer rankingSort;

}
