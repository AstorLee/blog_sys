package com.example.blog.sys.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2020/2/28 17:18
 **/

@Data
public class AuthorVO {
    private String userId;

    @JsonProperty("avatar")
    private String userAvatar;

    @JsonProperty("nickName")
    private String userNickname;

    public AuthorVO(String userId, String userAvatar, String userNickname) {
        this.userId = userId;
        this.userAvatar = userAvatar;
        this.userNickname = userNickname;
    }
}
