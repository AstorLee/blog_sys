package com.example.blog.sys.service;

import com.example.blog.sys.dao.User;
import com.example.blog.sys.vo.save.UserSaveVO;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/22 17:51
 **/

public interface UserService {

    User findByUserAccount(String account);

    User findByUserId(String userId);

    User findByUserEmail(String email);

    User findByUserPhone(Integer phone);

    Boolean userActivate(String id,String code);

    Boolean activateEmailUser(String to, String subject, String content);

    User userSave(User user);

}
