package com.example.blog.sys.dao;

import com.example.blog.sys.enums.LikeTypeEnum;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/29 10:26
 **/

@Data
@Entity
@Table(name = "blog_sys_like")
public class Like {

    @Id
    @GeneratedValue(generator = "jpa-uuid",strategy = GenerationType.IDENTITY)
    @Column(length = 32)
    private String likeId;

    @NotEmpty(message = "来源用户编号不能为空")
    private String likeFrom;

    @NotEmpty(message = "目标编号不能为空")
    private String likeTo;

    @NotEmpty(message = "目标用户编号不能为空")
    private String likeToUserId;

    private String likeType = LikeTypeEnum.ARTICLE.getType();

}
