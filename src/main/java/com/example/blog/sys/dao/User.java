package com.example.blog.sys.dao;

import com.example.blog.sys.enums.UserStatusEnum;
import lombok.Data;
import lombok.Generated;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019-08-22 14:08
 **/

@Entity
@Data
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
@Table(name = "blog_sys_user")
public class User {

    @Id
    @GeneratedValue(generator = "jpa-uuid",strategy = GenerationType.IDENTITY)
    @Column(length = 32)
    private String userId;

    @NotEmpty(message = "用户账号不能为空！")
    @Size(min = 4,max = 8)
    private String userAccount;

    @NotEmpty(message = "密码不能为空！")
    private String userPwd;

    private String infoId;

    @NotEmpty(message = "邮箱不能为空！")
    @Email
    private String userEmail;

    private String userPhone;

    private Integer roleId;

    private String roleName;

    private String userCode;

    private String userStatus = UserStatusEnum.DISABLED.getStatus();

    private String userNickname;
}
