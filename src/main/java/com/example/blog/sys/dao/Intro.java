package com.example.blog.sys.dao;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019-08-22 13:13
 **/

@Entity
@Table(name = "blog_sys_article_intro")
@Data
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
public class Intro {

    @Id
    @GeneratedValue(generator = "jpa-uuid",strategy = GenerationType.IDENTITY)
    @Column(length = 32)
    private String introId;

    @NotEmpty(message = "简介标题不能为空")
    @Size(min = 1,max = 12)
    private String introTitle;

    private String introDescription;

    private Integer introLook = 0;

    private Integer introLike = 0;

    private Integer introComment = 0;

    private Integer introCollect = 0;


    private String introThumbnail;

    private String articleId;

    private Timestamp introCreateTime = new Timestamp(System.currentTimeMillis());

    private Integer themeId;

    private String userId;

    private String userAvatar;

    private String userNickname;

    private Integer rangeId;

    private String rangeName;

    private String themeName;
}
