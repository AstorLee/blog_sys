package com.example.blog.sys.service.Impl;

import com.example.blog.sys.dao.Range;
import com.example.blog.sys.repository.RangeRepository;
import com.example.blog.sys.vo.RangeVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: LetBeing
 * @Description : 范围dto
 * @Date: 2019/8/29 11:36
 **/

@Service
public class RangeServiceImpl {

    @Autowired
    private RangeRepository repository;

    public List<RangeVO> findAll() {

        List<Range> rangeList =  repository.findAll();
        List<RangeVO> rangeVOList = new ArrayList<>();
        for (Range range:rangeList) {
            RangeVO rangeVO = new RangeVO();
            BeanUtils.copyProperties(range,rangeVO);
            rangeVOList.add(rangeVO);
        }
        return rangeVOList;
    }


}
