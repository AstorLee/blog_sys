package com.example.blog.sys.repository;

import com.example.blog.sys.dao.Collect;
import com.example.blog.sys.dao.CollectCate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author: LetBeing
 * @Description : 收藏
 * @Date: 2019/9/2 14:41
 **/

@Repository
public interface CollectRepository extends JpaRepository<Collect,String> {

    List<Collect> findAllByCateId(String cateId);

    List<Collect> findAllByCollectFrom(String fromId);

    Collect findByCollectId(String collectId);
}
