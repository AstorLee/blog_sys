package com.example.blog.sys.enums;

import lombok.Getter;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/30 14:50
 **/

@Getter
public enum FollowTypeEnum {
    /**
     * @return 关注类型enum
     * */
    TOUSER("1","关注用户"),
    TOARTICLE("2","关注文章"),
    TOSPECIAL("3","关注专栏"),
    TOTOPIC("4","关注话题");

    private final String type;
    private String remark;

    FollowTypeEnum(String type,String remark) {
        this.type = type;
        this.remark = remark;
    }
}
