package com.example.blog.sys.dao;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/27 15:12
 **/

@Entity
@Table(name = "blog_sys_user_special")
@Data
public class Special {

    @Id
    @GeneratedValue(generator = "jpa-uuid",strategy = GenerationType.IDENTITY)
    @Column(length = 32)
    private String specialId;

    @NotEmpty(message = "专栏名不能为空")
    private String specialName;

    private Integer articleNum = 0;

    private Integer followNum = 0;

    private String specialIcon;

    private Integer lookNum = 0;

    private String userId;
}
