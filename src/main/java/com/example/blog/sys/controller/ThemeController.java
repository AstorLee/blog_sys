package com.example.blog.sys.controller;

import com.example.blog.sys.service.Impl.ThemeServiceImpl;
import com.example.blog.sys.vo.ResultVO;
import com.example.blog.sys.vo.ThemeVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author: LetBeing
 * @Description : 主题管理
 * @Date: 2019/8/29 16:11
 **/

@RestController
@RequestMapping("/theme")
@Api(value = "theme",tags = "主题管理")
public class ThemeController {

    @Autowired
    ThemeServiceImpl themeService;

    @GetMapping("/{rangeId}")
    @ApiOperation(value = "根据rangeId获得所有主题")
    @ApiImplicitParam(name = "rangeId",value = "范围编号",required = true)
    public ResultVO findAllByRangeId(@PathVariable Integer rangeId) {
        List<ThemeVO> themeVOList = themeService.findAllByRangeId(rangeId);
        ResultVO resultVO = new ResultVO("成功",1000,themeVOList);
        return resultVO;
    }

    @GetMapping("")
    @ApiOperation(value = "查询所有主题")
    public ResultVO findAll(){
        return new ResultVO("成功",1000,themeService.findAll());
    }
}
