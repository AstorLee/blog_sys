package com.example.blog.sys.enums;

import lombok.Data;
import lombok.Getter;

/**
 * @Author: LetBeing
 * @Description : 全局异常处理
 * @Date: 2019/8/27 18:26
 **/

@Getter
public enum ErrorCodeEnum {

    /**
     * @return  错误类
     * */

    SUCCESS(1000,"成功"),
    OPERATIONERR(1001,"业务操作错误"),
    USERERR(1002,"用户错误操作"),
    AUTHERR(1003,"无权限"),
    NOTEXIST(1004,"不存在"),
    UNKNOWNERR(1005,"未知的错误");

    private final Integer code;

    private String remark;

    ErrorCodeEnum(Integer code,String remark) {
        this.code = code;
        this.remark = remark;
    }
}
