package com.example.blog.sys.controller;

import com.example.blog.sys.conf.MyException;
import com.example.blog.sys.dao.*;
import com.example.blog.sys.enums.ErrorCodeEnum;
import com.example.blog.sys.enums.EventsTypeEnum;
import com.example.blog.sys.note.RequireToken;
import com.example.blog.sys.service.*;
import com.example.blog.sys.service.Impl.RedisTemplateService;
import com.example.blog.sys.vo.ResultVO;
import com.example.blog.sys.vo.save.LikeSaveVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019-08-22 11:58
 **/

@RestController
@RequestMapping("/like")
@Api(value = "like", tags = "点赞管理")
public class LikeController {

    @Autowired
    private LikeService likeService;

    @Autowired
    private ArticleService articleService;

    @Autowired
    private FocusService focusService;

    @Autowired
    private IntroService introService;

    @Autowired
    private EventsService eventsService;

    @Autowired
    private RedisTemplateService redisTemplateService;

    @GetMapping("")
    @ApiOperation(value = "查询文章或评论的点赞总数")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "编号", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "type", value = "类型", example = "文章为0，评论为1", defaultValue = "0", paramType = "query", dataType = "String")
    })
    public ResultVO countArticleOrCommnetByLikeNum(@RequestParam String id, @RequestParam String type) {
        ResultVO resultVO = new ResultVO();
        Integer num;
        try {
            num = likeService.countLikeTo(id, type);
            resultVO.setCode(1000);
            resultVO.setMessage("成功");
            resultVO.setData(num);
        } catch (RuntimeException e) {
            e.printStackTrace();
            throw new MyException(ErrorCodeEnum.USERERR.getCode(), "参数错误");
        }

        return resultVO;
    }

    @PostMapping("")
    @RequireToken
    @ApiOperation(value = "点赞")
    @Transactional(rollbackFor = Exception.class)
    public ResultVO saveLike(@RequestBody LikeSaveVO likeSaveVO, HttpServletRequest request) {
        String jwt = request.getHeader("jwt");
        String key = "user_" + jwt;
        User user = redisTemplateService.get(key, User.class);
        if (user != null) {
            Like like = new Like();
            BeanUtils.copyProperties(likeSaveVO,like);
            like.setLikeFrom(user.getUserId());
            Boolean status = likeService.saveLike(like);
            //生成动态
            Events events = new Events();
            events.setEventsType(EventsTypeEnum.FROMLIKE.getType());
            events.setEventsFrom(user.getUserId());
            events.setEventsTo(like.getLikeToUserId());
            if ( "1".equals(likeSaveVO.getLikeType()) ) {
                Article article = articleService.findByArticleId(likeSaveVO.getLikeTo());
                article.setArticleLike(article.getArticleLike() + 1);
                articleService.saveArticle(article);
                Intro intro = introService.findByArticleId(likeSaveVO.getLikeTo());
                intro.setIntroLike(intro.getIntroLike() + 1);
                introService.saveIntro(intro);
                events.setEventsDescription(user.getUserNickname() +"点赞了你的文章");
            }else if ( "2".equals(likeSaveVO.getLikeType()) ){
                Focus focus = focusService.findByFocusId(likeSaveVO.getLikeTo());
                focus.setLikeNum(focus.getLikeNum() + 1);
                focusService.saveFocus(focus);
                events.setEventsDescription(user.getUserNickname() +"点赞了你的看点");
            }
            eventsService.saveEvents(events);
            return new ResultVO("成功", 1000, status);
        } else {
            throw new MyException(ErrorCodeEnum.USERERR.getCode(), "用户不存在");
        }
    }

    @DeleteMapping("/{id}")
    @RequireToken
    @ApiOperation(value = "按id删除点赞")
    @Transactional(rollbackFor = Exception.class)
    public ResultVO delLike(@PathVariable("id") String id, HttpServletRequest request) {
        String jwt = request.getHeader("jwt");
        String key = "user_" + jwt;
        User user = redisTemplateService.get(key, User.class);
        if (user != null) {
            Like like = likeService.findBuLikeId(id);
            if ( like != null ) {
                if (like.getLikeFrom().equals(user.getUserId())) {
                    Boolean status = likeService.delLike(id);
                    if ( "1".equals(like.getLikeType()) ) {
                        Article article = articleService.findByArticleId(like.getLikeTo());
                        article.setArticleLike(article.getArticleLike() - 1);
                        articleService.saveArticle(article);
                        Intro intro = introService.findByArticleId(like.getLikeTo());
                        intro.setIntroLike(intro.getIntroLike() - 1);
                        introService.saveIntro(intro);
                    }else if ( "2".equals(like.getLikeType()) ){
                        Focus focus = focusService.findByFocusId(like.getLikeTo());
                        focus.setLikeNum(focus.getLikeNum() - 1);
                        focusService.saveFocus(focus);
                    }
                    return new ResultVO("成功", 1000, status);
                } else {
                    throw new MyException(ErrorCodeEnum.USERERR.getCode(), "非法操作");
                }
            }else {
                throw new MyException(ErrorCodeEnum.NOTEXIST.getCode(), "记录不存在");
            }
        } else {
            throw new MyException(ErrorCodeEnum.USERERR.getCode(), "用户不存在");
        }
    }
}
