package com.example.blog.sys.enums;

import lombok.Getter;

/**
 * @Author: LetBeing
 * @Description : 积分变化类型
 * @Date: 2019/8/29 09:24
 **/

@Getter
public enum PointChangeTypeEnum {

    /**
     * @LOGIN 登录
     * @COMMENT 评论
     * @ADDARTICLE 添加文章
     * */
    LOGIN("1"),COMMENT("2"),ADDARTICLE("3"),PAY("4");

    private final String type;

    private Integer num;

    PointChangeTypeEnum(String type) {
        this.type = type;
        if ( "1".equals(type) ) {
            this.num = 10;
        }else if ( "2".equals(type) ) {
            this.num = 5;
        } else if ( "3".equals(type) ) {
            this.num = 20;
        }
    }
}
