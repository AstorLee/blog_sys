package com.example.blog.sys.repository;

import com.example.blog.sys.dao.Topic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/2 18:04
 **/

@Repository
public interface TopicRepository extends JpaRepository<Topic,Integer> {
    Topic findByTopicId(Integer id);
}
