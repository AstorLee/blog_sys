package com.example.blog.sys.service.Impl;

import com.example.blog.sys.dao.Theme;
import com.example.blog.sys.repository.ThemeRepository;
import com.example.blog.sys.vo.ThemeVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: LetBeing
 * @Description : 主题service
 * @Date: 2019/8/27 17:15
 **/

@Service
public class ThemeServiceImpl {

    @Autowired
    ThemeRepository repository;

    public List<ThemeVO> findAllByRangeId(Integer rangeId) {

        List<Theme> themeList = repository.findAllByRangeId(rangeId);
        List<ThemeVO> themeVOList = new ArrayList<>();

        for(Theme theme:themeList) {
            ThemeVO themeVO = new ThemeVO();
            BeanUtils.copyProperties(theme,themeVO);
            themeVOList.add(themeVO);
        }

        return themeVOList;
    }

    public List<ThemeVO> findAll(){
        List<Theme> themeList = repository.findAll();
        List<ThemeVO> themeVOList = new ArrayList<>();
        for (Theme theme:themeList) {
            ThemeVO themeVO = new ThemeVO();
            BeanUtils.copyProperties(theme,themeVO);
            themeVOList.add(themeVO);
        }
        return themeVOList;
    }

}
