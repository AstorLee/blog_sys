package com.example.blog.sys.controller;

import com.example.blog.sys.conf.MyException;
import com.example.blog.sys.dao.*;
import com.example.blog.sys.enums.ErrorCodeEnum;
import com.example.blog.sys.enums.EventsTypeEnum;
import com.example.blog.sys.note.RequireToken;
import com.example.blog.sys.service.EventsService;
import com.example.blog.sys.service.FocusService;
import com.example.blog.sys.service.FollowService;
import com.example.blog.sys.service.Impl.RedisTemplateService;
import com.example.blog.sys.service.Impl.TopicServiceImpl;
import com.example.blog.sys.service.UserInfoService;
import com.example.blog.sys.vo.FocusVO;
import com.example.blog.sys.vo.ResultVO;
import com.example.blog.sys.vo.save.FocusSaveVO;
import com.sun.tools.javac.util.ArrayUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: LetBeing
 * @Description : 看点管理
 * @Date: 2019/9/2 18:13
 **/

@RestController
@RequestMapping("/focus")
@Api(value = "focus",tags = "看点管理")
public class FocusController {

    @Autowired
    private FocusService focusService;

    @Autowired
    private RedisTemplateService redisTemplateService;

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private FollowService followService;

    @Autowired
    private EventsService eventsService;

    @Autowired
    private TopicServiceImpl topicService;

    @GetMapping("")
    @ApiOperation(value = "查询看点")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId",value = "用户编号",paramType = "query"),
            @ApiImplicitParam(name = "topicId",value = "话题编号",paramType = "query")
    })
    public ResultVO findAllById(@RequestParam(name = "userId",required = false) String userId,
                                @RequestParam(name = "topicId",required = false) Integer topicId,
                                HttpServletRequest request) {
        if ( userId != null ) {
            List<FocusVO> focusList = focusService.findAllByUserId(userId);
            return new ResultVO("成功",1000,focusList);
        }else if (topicId != null){
            List<FocusVO> focusList = focusService.findAllByTopicId(topicId);
            return new ResultVO("成功",1000,focusList);
        }else {
            if ( request.getHeader("jwt") != null ) {
                String key = "user_" + request.getHeader("jwt");
                User user = redisTemplateService.get(key,User.class);
                if ( user != null ) {
                    List<FocusVO> focusList = focusService.findAllByUserId(user.getUserId());
                    return new ResultVO("成功",1000,focusList);
                }else {
                    throw new MyException(ErrorCodeEnum.NOTEXIST.getCode(),"非法操作");
                }
            }else {
                throw new MyException(ErrorCodeEnum.NOTEXIST.getCode(),"无参数");
            }
        }
    }

    @PostMapping("")
    @ApiOperation(value = "新增看点")
    @RequireToken
    public ResultVO saveFocus(@RequestBody FocusSaveVO focusSaveVO, HttpServletRequest request) {
        Focus focus = new Focus();
        BeanUtils.copyProperties(focusSaveVO,focus);
        String jwt = request.getHeader("jwt");
        String key  = "user_" + jwt;
        UserInfo userInfo = redisTemplateService.get(key, UserInfo.class);
        if ( userInfo != null ) {
            focus.setUserId(userInfo.getUserId());
            focus.setUserAvatar(userInfo.getInfoAvatar());
            focus.setUserNickname(userInfo.getInfoNickname());
            if ( focusSaveVO.getImgList() != null && focusSaveVO.getImgList().size() != 0 ) {
                StringBuffer stringBuffer = new StringBuffer();
                for ( String s:focusSaveVO.getImgList() ) {
                    stringBuffer.append(s + ",");
                }
                focus.setFocusImgs(stringBuffer.toString());
            }
            boolean status = focusService.saveFocus(focus);
            //话题增加看点数
            if ( status ) {
                if ( focus.getTopicId() != null ) {
                    Topic topic = topicService.findById(focus.getTopicId());
                    topic.setFocusNum(topic.getFocusNum() + 1 );
                    topicService.saveTopic(topic);
                }
            }
            //生成动态
            List<Follow> followList = followService.findAllByFollowTo(userInfo.getUserId());
            List<Events> eventsList = new ArrayList<Events>();
            for ( Follow follow:followList ) {
                Events events = new Events();
                events.setEventsType(EventsTypeEnum.FROMFOLLOW.getType());
                events.setEventsFrom(userInfo.getUserId());
                events.setEventsTo(follow.getFollowFrom());
                events.setEventsDescription("你关注的" + userInfo.getInfoNickname() + "发布了新的看点");
                eventsList.add(events);
            }
            eventsService.saveAll(eventsList);
            return new ResultVO("成功",1000,status);
        }else {
            throw new MyException(ErrorCodeEnum.USERERR.getCode(),"账号不存在");
        }
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除看点")
    @RequireToken
    public ResultVO delFocus(@PathVariable("id") String id,HttpServletRequest request) {
        String jwt = request.getHeader("jwt");
        String key = "user_" + jwt;
        User user = redisTemplateService.get(key,User.class);
        Focus focus = focusService.findByFocusId(id);
        if ( focus != null ) {
            if (focus.getUserId().equals(user.getUserId())) {
                focusService.delFocus(id);
                Topic topic = topicService.findById(focus.getTopicId());
                topic.setFocusNum( topic.getFollowNum() - 1 );
                topicService.saveTopic(topic);
                return new ResultVO("成功",1000,true);
            }else {
                throw new MyException(ErrorCodeEnum.USERERR.getCode(),"非法操作");
            }
        }else {
            throw new MyException(ErrorCodeEnum.USERERR.getCode(),"看点不存在");
        }
    }
}
