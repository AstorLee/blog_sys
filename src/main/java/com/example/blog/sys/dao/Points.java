package com.example.blog.sys.dao;

import com.example.blog.sys.enums.PointsTypeEnum;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/22 17:09
 **/

@Entity
@Data
@Table(name = "blog_sys_user_points")
public class Points {

    @Id
    @GeneratedValue(generator = "jpa-uuid",strategy = GenerationType.IDENTITY)
    @Column(length = 32)
    private String pointsId;

    @NotEmpty(message = "积分数值不能为空")
    private Integer pointsNum;

    private Integer pointsType = PointsTypeEnum.ADD.getType();

    private String userId;

}
