package com.example.blog.sys.service;

import com.example.blog.sys.dao.Intro;
import com.example.blog.sys.vo.AuthorVO;
import com.example.blog.sys.vo.IntroVO;
import com.example.blog.sys.vo.PageVO;
import org.springframework.data.domain.Pageable;
import sun.jvm.hotspot.debugger.Page;

import java.util.List;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/26 16:55
 **/

public interface IntroService {

    Intro saveIntro(Intro intro);

    Boolean delIntro(String introId);

    Boolean delByArticleId(String articleId);

    List<IntroVO> findAll();

    /**
     * @param themeId 主题编号
     * @param pageable 分页对象
     * */
    PageVO findAllByThemeId(Integer themeId, Pageable pageable);

    /**
     * @param userId 用户编号或者被关注者编号
     * */
    List<IntroVO> findAllByUserId(String userId);

    Intro findById(String id);

    Intro findByArticleId(String articleId);

    /**
     * 根据rangeId查找所有文章
     * */
    PageVO findByRangeId(Integer rangeId,Pageable pageable);

    /**
     * 查找关注的人的文章
     * */
    List<IntroVO> findAllByFollow(String userId);

    /**
     * 查询最热的文章（点赞对多的）
     * */
//    PageVO findAllByHot(Pageable pageable);

}
