package com.example.blog.sys.service;

import com.example.blog.sys.dao.Article;
import com.example.blog.sys.vo.ArticleVO;
import com.example.blog.sys.vo.IntroVO;
import com.example.blog.sys.vo.save.ArticleSaveVO;

import java.util.List;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/22 18:40
 **/

public interface ArticleService {

    Article saveArticle(Article article);

    Boolean delArticle(String id);

    Article findByArticleId(String articleId);
}
