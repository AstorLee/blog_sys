package com.example.blog.sys.repository;

import com.example.blog.sys.dao.Events;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/6 15:01
 **/

@Repository
public interface EventsRepository extends JpaRepository<Events, String> {

    List<Events> findAllByEventsTo(String eventsTo);

    Integer deleteAllByEventsTo(String eventsTo);

    Events findByEventsId(String eventsId);
}
