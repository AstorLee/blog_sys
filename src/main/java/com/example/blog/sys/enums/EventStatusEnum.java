package com.example.blog.sys.enums;

import lombok.Getter;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/6 14:58
 **/

@Getter
public enum EventStatusEnum {
    /**
     *
     * */
    READ("1","已读"),UNREAD("2","未读");

    private String status;

    private String msg;

    EventStatusEnum(String status, String msg) {
        this.status = status;
        this.msg = msg;
    }
}
