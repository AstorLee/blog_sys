package com.example.blog.sys.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/23 11:33
 **/

@Data
public class LoginVO {

    @JsonProperty("account")
    @NotEmpty(message = "账号不能为空")
    @NotNull(message = "账号不能为空")
    private String userAccount;

    @JsonProperty("pwd")
    @NotEmpty(message = "密码不能为空")
    @NotNull(message = "密码不能为空")
    private String userPwd;
}
