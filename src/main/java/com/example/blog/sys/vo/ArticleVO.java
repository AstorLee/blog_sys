package com.example.blog.sys.vo;

import com.example.blog.sys.dao.Theme;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/27 15:02
 **/

@Data
public class ArticleVO implements Serializable {

    @JsonProperty("id")
    private String articleId;

    @JsonProperty("title")
    private String articleTitle;

    @JsonProperty("content")
    private String articleContent;

    private List<Theme> themeList;

    private SpecialVO special;

    private String userId;

    @JsonProperty("createTime")
    private Timestamp articleCreateTime;

    @JsonProperty("look")
    private Integer articleLook;

    @JsonProperty("collect")
    private Integer articleCollect;

    @JsonProperty("like")
    private Integer articleLike;

    @JsonProperty("comment")
    private Integer articleComment;

    private Integer likeStatus = 0;

    private String likeId;

    @JsonProperty("userInfo")
    private UserInfoVO userInfoVO;

    private Boolean followStatus = false;

    private String followId;

}
