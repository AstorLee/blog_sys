package com.example.blog.sys.service.Impl;

import com.example.blog.sys.dao.Like;
import com.example.blog.sys.repository.LikeRepository;
import com.example.blog.sys.service.LikeService;
import com.example.blog.sys.vo.save.LikeSaveVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/29 10:43
 **/

@Service
public class LikeServiceImpl implements LikeService {

    @Autowired
    private LikeRepository repository;

    @Override
    public Integer countLikeTo(String articleId, String likeType) {
        return repository.countAllByLikeToAndLikeType(articleId,likeType);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveLike(Like like) {
        try {
            repository.save(like);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public Like findByLikeFromAndTo(String formId, String to) {
        return repository.findByLikeFromAndLikeTo(formId,to);
    }

    @Override
    public Like findBuLikeId(String likeId) {
        return repository.findByLikeId(likeId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean delLike(String likeId) {
        try {
            repository.deleteById(likeId);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public Like findByLikeFrom(String formId) {
        return repository.findByLikeFrom(formId);
    }
}
