package com.example.blog.sys.enums;

import lombok.Getter;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/11 14:37
 **/

@Getter
public enum RankingTypeEnum {

    /***/

    AUTHOR("1","作者榜"),HOT("2","热门榜"),NEWS("3","最新");

    private String type;

    private String msg;

    RankingTypeEnum(String type, String msg) {
        this.type = type;
        this.msg = msg;
    }
}
