package com.example.blog.sys.controller;

import com.example.blog.sys.dao.Topic;
import com.example.blog.sys.note.RequireRole;
import com.example.blog.sys.note.RequireToken;
import com.example.blog.sys.service.Impl.TopicServiceImpl;
import com.example.blog.sys.vo.ResultVO;
import com.example.blog.sys.vo.save.TopicSaveVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author: LetBeing
 * @Description : 话题管理
 * @Date: 2019/9/2 18:27
 **/

@RestController
@RequestMapping("/topic")
@Api(value = "topic" ,tags = "话题管理")
public class TopicController {

    @Autowired
    private TopicServiceImpl topicService;

    @GetMapping("")
    @ApiOperation(value = "查询所有话题")
    public ResultVO findAllTopic() {
        return new ResultVO("成功",1000,topicService.findAll());
    }

    @PostMapping("")
    @ApiOperation(value = "新增话题")
    @RequireToken
    @RequireRole
    public ResultVO saveTopic(@RequestBody TopicSaveVO topicSaveVO) {
        Topic topic = new Topic();
        BeanUtils.copyProperties(topicSaveVO,topic);
        return new ResultVO("成功",1000,topicService.saveTopic(topic));
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除话题")
    @RequireToken
    @RequireRole
    public ResultVO delTopic(@PathVariable("id") Integer id) {
        return new ResultVO("成功",1000,topicService.delTopicByTopicId(id));
    }

}
