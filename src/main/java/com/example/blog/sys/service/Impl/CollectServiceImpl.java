package com.example.blog.sys.service.Impl;

import com.example.blog.sys.dao.Collect;
import com.example.blog.sys.repository.CollectRepository;
import com.example.blog.sys.service.CollectService;
import com.example.blog.sys.vo.CollectVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: LetBeing
 * @Description : 收藏service
 * @Date: 2019/9/2 15:44
 **/

@Service
public class CollectServiceImpl implements CollectService {

    @Autowired
    private CollectRepository repository;

    @Override
    public List<CollectVO> findAllByCateId(String cateId) {
        List<Collect> collectList = repository.findAllByCateId(cateId);
        List<CollectVO> collectVOList = new ArrayList<>();
        for (Collect collect:collectList) {
            CollectVO collectVO = new CollectVO();
            BeanUtils.copyProperties(collect,collectVO);
            collectVOList.add(collectVO);
        }
        return collectVOList;
    }

    @Override
    public List<CollectVO> findAllByCollectFrom(String fromId) {
        List<Collect> collectList = repository.findAllByCollectFrom(fromId);
        List<CollectVO> collectVOList = new ArrayList<>();
        for (Collect collect:collectList) {
            CollectVO collectVO = new CollectVO();
            BeanUtils.copyProperties(collect,collectVO);
            collectVOList.add(collectVO);
        }
        return collectVOList;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveCollect(Collect collect) {
        try {
            repository.save(collect);
        }catch ( Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean delCollect(String collectId) {
        try {
            repository.deleteById(collectId);
        }catch ( Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public Collect findById(String id) {
        return repository.findByCollectId(id);
    }
}
