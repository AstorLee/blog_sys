package com.example.blog.sys.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/11 15:40
 **/

@Data
public class CommentVO {

    @JsonProperty("id")
    private String commentId;

    @JsonProperty("content")
    private String commentContent;

    @JsonProperty("from")
    private String commentFrom;

    @JsonProperty("from_nickName")
    private String commentFromNickname;

    @JsonProperty("from_avatar")
    private String commentFromAvatar;

    @JsonProperty("toUserId")
    private String commentToUserId;

    @JsonProperty("createTime")
    private Timestamp commentCreateTime;

    @JsonProperty("type")
    private String commentType;

    @JsonProperty("sid")
    private String commentSid;

    @JsonProperty("toName")
    private String commentToNickname;

    @JsonProperty("focusId")
    private String focusId;

    private List<CommentVO> replyList = new ArrayList<>();

}