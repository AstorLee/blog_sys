package com.example.blog.sys.controller;

import com.example.blog.sys.conf.MyException;
import com.example.blog.sys.dao.CollectCate;
import com.example.blog.sys.dao.User;
import com.example.blog.sys.enums.ErrorCodeEnum;
import com.example.blog.sys.note.RequireToken;
import com.example.blog.sys.service.CollectCateService;
import com.example.blog.sys.service.Impl.RedisTemplateService;
import com.example.blog.sys.vo.CollectCateVO;
import com.example.blog.sys.vo.ResultVO;
import com.example.blog.sys.vo.save.CollectCateSaveVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/2 15:08
 **/

@RestController
@RequestMapping("/collect/cate")
@Api(tags = "收藏分类管理",value = "collectCate")
public class CollectCateController {

    @Autowired
    private CollectCateService collectCateService;

    @Autowired
    RedisTemplateService redisTemplateService;

    @GetMapping("")
    @ApiOperation(value = "查询用户所有收藏分类")
    @RequireToken
    public ResultVO findAllByUser(HttpServletRequest request) {
        String jwt = request.getHeader("jwt");
        String key = "user_" + jwt;
        User user = redisTemplateService.get(key,User.class);
        List<CollectCateVO> collectCateVOList;
        if ( user != null ) {
            collectCateVOList = collectCateService.findAllByUserId(user.getUserId());
        }else {
            throw new MyException(ErrorCodeEnum.NOTEXIST.getCode(),"查无此人");
        }
        return new ResultVO("成功",1000,collectCateVOList);
    }

    @PostMapping("")
    @ApiOperation(value = "保存收藏分类")
    @RequireToken
    public ResultVO saveCollectCate(@RequestBody CollectCateSaveVO collectCateSaveVO,HttpServletRequest request) {
        String jwt = request.getHeader("jwt");
        String key = "user_" + jwt;
        User user = redisTemplateService.get(key,User.class);
        if ( user != null ) {
            CollectCate collectCate = new CollectCate();
            collectCate.setCateName(collectCateSaveVO.getCateName());
            collectCate.setUserId(user.getUserId());
            collectCateService.saveCollectCate(collectCate);
            return new ResultVO("成功",1000,true);
        }else {
            throw new MyException(ErrorCodeEnum.USERERR.getCode(),"用户不存在");
        }
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "按分类编号删除分类")
    @ApiImplicitParam(value = "id",name = "分类id")
    @RequireToken
    public ResultVO delCollectCate(@PathVariable("id") String id, HttpServletRequest request) {
        String jwt = request.getHeader("jwt");
        String key = "user_" + jwt;
        User user = redisTemplateService.get(key,User.class);
        CollectCate collectCate = collectCateService.findById(id);
        if ( collectCate != null ) {
            if (collectCate.getUserId().equals(user.getUserId())) {
                collectCateService.delCollectCate(id);
                return new ResultVO("成功",1000,true);
            }else {
                throw new MyException(ErrorCodeEnum.USERERR.getCode(),"非法操作");
            }
        }else {
            throw new MyException(ErrorCodeEnum.USERERR.getCode(),"分类不存在");
        }
    }

}
