package com.example.blog.sys.dao;

import com.example.blog.sys.enums.EventStatusEnum;
import com.example.blog.sys.enums.EventsTypeEnum;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.sql.Timestamp;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/6 14:45
 **/

@Entity
@Data
@Table(name = "blog_sys_user_events")
@GenericGenerator(name = "jpa-uuid",strategy = "uuid")
public class Events {

    @Id
    @GeneratedValue(generator = "jpa-uuid",strategy = GenerationType.IDENTITY)
    @Column(length = 32)
    private String eventsId;

    private String eventsType = EventsTypeEnum.FROMFOLLOW.getType();

    private String eventsFrom;

    @NotEmpty(message = "目标编号不能为空")
    private String eventsTo;

    private String eventsStatus = EventStatusEnum.UNREAD.getStatus();

    private Timestamp eventsCreateTime = new Timestamp(System.currentTimeMillis());

    private String eventsDescription;

    private String eventsTouch;

}
