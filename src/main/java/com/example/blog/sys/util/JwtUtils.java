package com.example.blog.sys.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.example.blog.sys.dao.User;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/23 10:53
 **/

public class JwtUtils {

    public String getToken(User user) {
        String token = "";
        token = JWT.create().withAudience(user.getUserId())
                .sign(Algorithm.HMAC256(user.getUserPwd()));
        return token;
    }

}
