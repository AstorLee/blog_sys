package com.example.blog.sys.service.Impl;

import com.example.blog.sys.dao.UserInfo;
import com.example.blog.sys.repository.UserInfoRepository;
import com.example.blog.sys.service.UserInfoService;
import com.example.blog.sys.vo.UserInfoVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/23 16:28
 **/

@Service
public class UserInfoServiceImpl implements UserInfoService {

    @Autowired
    UserInfoRepository repository;


    @Override
    public UserInfo updateUserInfo(UserInfo userInfo) {
        return repository.save(userInfo);
    }

    @Override
    public UserInfo saveUserInfo(UserInfo userInfo) {
        return repository.save(userInfo);
    }

    @Override
    public UserInfo findByUserId(String userId) {
        return repository.findByUserId(userId);
    }

    @Override
    public UserInfo findByInfoId(String infoId) {
        return repository.findByInfoId(infoId);
    }
}
