package com.example.blog.sys.vo.save;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/11 15:49
 **/

@Data
public class CommentSaveVO {

    @JsonProperty("content")
    private String commentContent;

    @JsonProperty("to")
    private String commentToUserId;

    @JsonProperty("type")
    private String commentType;

    @JsonProperty("sid")
    private String commentSid;

    private String articleId;

    private String focusId;

    @JsonProperty("toName")
    private String commentToNickname;
}
