package com.example.blog.sys.vo;

import lombok.Data;

import java.util.List;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/30 15:46
 **/

@Data
public class PageVO {

    private Integer offset;

    private Integer limit;

    private Integer total;

    private List list;
}
