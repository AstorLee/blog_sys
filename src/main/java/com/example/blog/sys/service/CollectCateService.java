package com.example.blog.sys.service;

import com.example.blog.sys.dao.CollectCate;
import com.example.blog.sys.vo.CollectCateVO;

import java.util.List;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/2 15:01
 **/

public interface CollectCateService {

    List<CollectCateVO> findAllByUserId(String userId);

    Boolean saveCollectCate(CollectCate collectCate);

    Boolean delCollectCate(String collectCateId);

    CollectCate findById(String id);

}
