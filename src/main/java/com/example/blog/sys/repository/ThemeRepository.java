package com.example.blog.sys.repository;

import com.example.blog.sys.dao.Theme;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author: LetBeing
 * @Description : 主题dto
 * @Date: 2019/8/27 17:12
 **/
@Repository
public interface ThemeRepository extends JpaRepository<Theme,String> {

    List<Theme> findAllByRangeId(Integer rangeId);

}
