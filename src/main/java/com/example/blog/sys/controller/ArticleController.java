package com.example.blog.sys.controller;

import com.example.blog.sys.conf.MyException;
import com.example.blog.sys.dao.*;
import com.example.blog.sys.enums.ErrorCodeEnum;
import com.example.blog.sys.enums.EventsTypeEnum;
import com.example.blog.sys.note.RequireToken;
import com.example.blog.sys.repository.IntroRepository;
import com.example.blog.sys.service.*;
import com.example.blog.sys.service.Impl.RedisTemplateService;
import com.example.blog.sys.vo.*;
import com.example.blog.sys.vo.save.ArticleSaveVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.management.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019-08-22 11:53
 **/

@RestController
@RequestMapping("/article")
@Api(value = "文章管理", tags = "文章管理")
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @Autowired
    private IntroService introService;

    @Autowired
    RedisTemplateService redisTemplateService;

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private FollowService followService;

    @Autowired
    private LikeService likeService;

    @Autowired
    private EventsService eventsService;

    @Resource
    private IntroRepository introRepository;

    @ApiOperation(value = "根据themeId查询文章列表")
    @GetMapping("")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "themeId", value = "主题编号", paramType = "query"),
            @ApiImplicitParam(name = "offset", value = "当前页码", paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "每页条数", paramType = "query")
    })
    public ResultVO findAllArticleBy(
            @RequestParam(value = "themeId", required = false) Integer themeId,
            @RequestParam(value = "rangeId", required = false) Integer rangeId,
            @RequestParam(value = "offset", required = false, defaultValue = "1") Integer offset,
            @RequestParam(value = "limit", required = false, defaultValue = "10") Integer limit,
            HttpServletRequest request) {
        PageVO pageVO = new PageVO();
        if (themeId != null) {
            if ( themeId == 0 ) {
                PageRequest pageRequest = PageRequest.of(offset - 1, limit, Sort.Direction.DESC, "introCreateTime");
//                pageVO = introService.findAllByHot(pageRequest);
            }else {
                PageRequest pageRequest = PageRequest.of(offset - 1, limit, Sort.Direction.DESC, "introCreateTime");
                pageVO = introService.findAllByThemeId(themeId, pageRequest);
            }
        } else {
            if ( rangeId != null ) {
                PageRequest pageRequest = PageRequest.of(offset - 1, limit, Sort.Direction.DESC, "introCreateTime");
                pageVO = introService.findByRangeId(rangeId,pageRequest);
            }else {
                throw new MyException(ErrorCodeEnum.OPERATIONERR.getCode(),"参数错误");
            }
        }
        ResultVO resultVO = new ResultVO("成功", 1000, pageVO);
        return resultVO;
    }


    @ApiOperation(value = "保存文章")
    @PostMapping("")
    @Transactional(rollbackFor = Exception.class)
    @RequireToken
    public ResultVO saveArticle(@RequestBody ArticleSaveVO articleSaveVO,HttpServletRequest request) {

        String key = "user_" + request.getHeader("jwt");
        User user = redisTemplateService.get(key, User.class);

        if ( user != null ) {
            UserInfo userInfo = userInfoService.findByUserId(user.getUserId());

            //创建文章及文章简介
            Article article = new Article();
            BeanUtils.copyProperties(articleSaveVO, article);
            article.setUserId(user.getUserId());
            Article newArticle = articleService.saveArticle(article);
            Intro intro = new Intro();
            intro.setThemeId(articleSaveVO.getThemeId());
            intro.setThemeName(articleSaveVO.getThemeName());
            intro.setArticleId(newArticle.getArticleId());
            intro.setIntroTitle(newArticle.getArticleTitle());
            intro.setIntroDescription(articleSaveVO.getDescription());
            intro.setRangeId(articleSaveVO.getRangeId());
            intro.setRangeName(articleSaveVO.getRangeName());
            intro.setUserId(user.getUserId());
            intro.setUserNickname(userInfo.getInfoNickname());
            Intro newIntro = introService.saveIntro(intro);
            newArticle.setIntroId(newIntro.getIntroId());
            articleService.saveArticle(newArticle);
            //生成动态
            List<Follow> followList = followService.findAllByFollowTo(user.getUserId());
            List<Events> eventsList = new ArrayList<Events>();
            for ( Follow follow:followList ) {
                Events events = new Events();
                events.setEventsType(EventsTypeEnum.FROMFOLLOW.getType());
                events.setEventsFrom(user.getUserId());
                events.setEventsTo(follow.getFollowFrom());
                events.setEventsDescription("你关注的" + user.getUserNickname() + "发布了新的文章");
                events.setEventsTouch(newArticle.getArticleId());
                eventsList.add(events);
            }
            eventsService.saveAll(eventsList);
        }else {
            throw  new MyException(ErrorCodeEnum.USERERR.getCode(),"非法操作");
        }
        ResultVO resultVO = new ResultVO("成功", 1000, true);
        return resultVO;
    }

    @ApiOperation(value = "删除文章")
    @DeleteMapping("/{id}")
    @RequireToken
    @Transactional(rollbackFor = Exception.class)
    public ResultVO delArticle(@PathVariable("id") String id) {
        articleService.delArticle(id);
        introService.delByArticleId(id);
        ResultVO resultVO = new ResultVO("成功", 1000, true);
        return resultVO;
    }

    @ApiOperation(value = "按articleId查询文章")
    @GetMapping("/{id}")
    public ResultVO findByArticleId(@PathVariable("id") String id,HttpServletRequest request) {
        /**
         * 查文章，添加文章浏览量并且判断是否本人点击，本人点击不计入浏览量
         *
         * */
        try {
            Article article = articleService.findByArticleId(id);
            UserInfo userInfo = userInfoService.findByUserId(article.getUserId());
            UserInfoVO userInfoVO = new UserInfoVO();
            BeanUtils.copyProperties(userInfo,userInfoVO);
            ArticleVO articleVO = new ArticleVO();
            BeanUtils.copyProperties(article, articleVO);
            articleVO.setUserInfoVO(userInfoVO);

            String key = "user_" + request.getHeader("jwt");
            User user = redisTemplateService.get(key,User.class);
            if ( user != null ) {
                Like like = likeService.findByLikeFromAndTo(user.getUserId(),id);
                if ( like != null ) {
                    articleVO.setLikeStatus(1);
                    articleVO.setLikeId(like.getLikeId());
                }
                Follow follow = followService.findByFollowFromAndFollowTO(user.getUserId(),article.getUserId());
                if ( follow != null ) {
                    articleVO.setFollowStatus(true);
                    articleVO.setFollowId(follow.getFollowId());
                }
                if ( !user.getUserId().equals(article.getUserId()) ) {
                    //非本人浏览增加点击量
                    article.setArticleLook(article.getArticleLook() + 1);
                    articleService.saveArticle(article);
                    Intro intro = introService.findById(article.getIntroId());
                    intro.setIntroLook(article.getArticleLook());
                    introService.saveIntro(intro);
                }
            }
            ResultVO resultVO = new ResultVO("成功", 1000, articleVO);
            return resultVO;
        }catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ErrorCodeEnum.NOTEXIST.getCode(),"文章不存在");
        }
    }

    @ApiOperation("查询作者榜")
    @GetMapping("/authors")
    public ResultVO findAuthor() {
        List<AuthorVO> authorVOList = introRepository.findAuthorList();
        ResultVO resultVO = new ResultVO("成功", 1000, authorVOList.subList(0,5));
        return resultVO;
    }
}
