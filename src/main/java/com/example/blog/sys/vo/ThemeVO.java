package com.example.blog.sys.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/27 15:26
 **/

@Data
public class ThemeVO {

    @JsonProperty("id")
    private Integer themeId;

    @JsonProperty("name")
    private String themeName;

    @JsonProperty("num")
    private Integer themeNum;

    @JsonProperty("rangeId")
    private Integer rangeId;
}
