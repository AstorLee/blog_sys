package com.example.blog.sys.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.example.blog.sys.conf.MyException;
import com.example.blog.sys.dao.Role;
import com.example.blog.sys.dao.User;
import com.example.blog.sys.enums.ErrorCodeEnum;
import com.example.blog.sys.note.PassToken;
import com.example.blog.sys.note.RequireRole;
import com.example.blog.sys.note.RequireToken;
import com.example.blog.sys.repository.RoleRepository;
import com.example.blog.sys.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

/**
 * @Author: LetBeing
 * @Description : 拦截管理
 * @Date: 2019/8/23 11:06
 **/

public class AuthenticationInterceptor implements HandlerInterceptor {

    @Autowired
    UserService userService;

    @Autowired
    RoleRepository roleRepository;

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object object) throws Exception {
        //从请求头取出token
        String token = httpServletRequest.getHeader("jwt");
        //如果不是映射到方法直接通过
        if (!(object instanceof HandlerMethod) ) {
            return true;
        }
        HandlerMethod handlerMethod = (HandlerMethod)object;
        Method method = handlerMethod.getMethod();
        //检查如果有passToken注解，有则跳过认证
        if ( method.isAnnotationPresent(PassToken.class) ) {
            PassToken passToken = method.getAnnotation(PassToken.class);
            if ( passToken.required() ) {
                return true;
            }
        }
        //检查有没需要用户权限的注解
        if ( method.isAnnotationPresent(RequireToken.class) ) {
            RequireToken requireToken = method.getAnnotation(RequireToken.class);
            if ( requireToken.required() ) {
                if ( token == null ) {
                    throw new MyException(ErrorCodeEnum.USERERR.getCode(),"无token,请重新登录");
                }
                String userId;
                try {
                    userId = JWT.decode(token).getAudience().get(0);
                }catch (JWTDecodeException j) {
                    throw new MyException(ErrorCodeEnum.NOTEXIST.getCode(),"找不到用户编号");
                }
                User user = userService.findByUserId(userId);
                if ( user == null ) {
                    throw new MyException(ErrorCodeEnum.NOTEXIST.getCode(),"找不到用户编号");
                }
                JWTVerifier jwtVerifier = JWT.require(Algorithm.HMAC256(user.getUserPwd())).build();
                try {
                    jwtVerifier.verify(token);
                }catch (JWTVerificationException e) {
                    throw new MyException(ErrorCodeEnum.USERERR.getCode(),"用户密码不匹配");
                }
            }
        }
        //检查是否需要管理员身份
        if ( method.isAnnotationPresent(RequireRole.class) ) {
            RequireRole requireRole = method.getAnnotation(RequireRole.class);
            if ( requireRole.required() ) {
                String userId = JWT.decode(token).getAudience().get(0);
                User user = userService.findByUserId(userId);
                Role role = roleRepository.findByRoleId(user.getRoleId());
                if ( role.getRoleCode() == 10 ) {
                    throw new MyException(ErrorCodeEnum.AUTHERR.getCode(),"无权限");
                }
            }
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
