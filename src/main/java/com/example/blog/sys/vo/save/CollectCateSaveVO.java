package com.example.blog.sys.vo.save;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/2 15:12
 **/

@Data
public class CollectCateSaveVO {

    @JsonProperty("name")
    private String cateName;

}
