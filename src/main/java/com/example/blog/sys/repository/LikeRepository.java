package com.example.blog.sys.repository;

import com.example.blog.sys.dao.Like;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/29 10:32
 **/

public interface LikeRepository extends JpaRepository<Like,String> {

    /**
     * @param likeTo 文章编号或者评论编号
     * @param likeType 点赞类型，点赞评论或文章
     * @return 数值
     * */
    Integer countAllByLikeToAndLikeType(String likeTo,String likeType);

    Like findByLikeFrom(String from);

    Like findByLikeFromAndLikeTo(String from,String to);

    Like findByLikeId(String id);

}
