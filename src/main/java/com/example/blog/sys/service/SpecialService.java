package com.example.blog.sys.service;

import com.example.blog.sys.dao.Special;
import com.example.blog.sys.vo.SpecialVO;

import java.util.List;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/27 17:07
 **/

public interface SpecialService {

    Boolean saveSpecial(Special special);

    Boolean delSpecial(String specialId);

    List<SpecialVO> findAllByUserId(String userId);

    Special findBySpecialId(String specialId);

}
