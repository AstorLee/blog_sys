package com.example.blog.sys.service.Impl;

import com.example.blog.sys.dao.Follow;
import com.example.blog.sys.repository.FollowRepository;
import com.example.blog.sys.service.FollowService;
import com.example.blog.sys.vo.FollowVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/30 15:04
 **/

@Service
public class FollowServiceImpl implements FollowService {

    @Autowired
    FollowRepository repository;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean delFollow(String id) {
        try {
            repository.deleteById(id);
        } catch ( Exception e ) {
            return false;
        }
        return true;
    }

    @Override
    public Follow findByFollowFromAndFollowTO(String follow, String to) {
        return repository.findByFollowFromAndFollowTo(follow,to);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveFollow(Follow follow) {
        try {
            repository.save(follow);
        }catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public List<Follow> findAllByFollowTo(String to) {
        return repository.findAllByFollowTo(to);
    }

    @Override
    public List<FollowVO> findAllByFollowTypeAndFollowFrom(String type,String userId) {
        List<Follow> followList = repository.findAllByFollowFromAndFollowType(userId,type);
        List<FollowVO> followVOList = new ArrayList<>();
        for (Follow follow:followList) {
            FollowVO followVO = new FollowVO();
            BeanUtils.copyProperties(follow,followVO);
            followVOList.add(followVO);
        }
        return followVOList;
    }

    @Override
    public List<FollowVO> findAllByFollowType(String type) {
        List<Follow> followList = repository.findAllByFollowType(type);
        List<FollowVO> followVOList = new ArrayList<>();
        for (Follow follow:followList) {
            FollowVO followVO = new FollowVO();
            BeanUtils.copyProperties(follow,followVO);
            followVOList.add(followVO);
        }
        return followVOList;
    }

    @Override
    public Follow findByFollowId(String followId) {
        return repository.findByFollowId(followId);
    }
}
