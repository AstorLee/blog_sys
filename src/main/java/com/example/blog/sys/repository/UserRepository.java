package com.example.blog.sys.repository;

import com.example.blog.sys.dao.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/22 17:29
 **/

public interface UserRepository extends JpaRepository<User,String> {

    User findByUserAccount(String account);

    User findByUserId(String userId);

    User findByUserEmail(String email);

    User findByUserPhone(Integer phone);

}
