package com.example.blog.sys.service.Impl;

import com.example.blog.sys.dao.Events;
import com.example.blog.sys.repository.EventsRepository;
import com.example.blog.sys.service.EventsService;
import com.example.blog.sys.vo.EventsVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/6 15:19
 **/

@Service
public class EventsServiceImpl implements EventsService {

    @Autowired
    private EventsRepository repository;

    @Override
    public boolean saveAll(List<Events> eventsList) {
        try {
            repository.saveAll(eventsList);
        }catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean delAll(String eventsTo) {
        try {
            repository.deleteAllByEventsTo(eventsTo);
        }catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean saveEvents(Events events) {
        try {
            repository.save(events);
        }catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public List<EventsVO> findAllEventsVO(String eventsTo) {
        List<Events> eventsList = repository.findAllByEventsTo(eventsTo);
        List<EventsVO> eventsVOList = new ArrayList<EventsVO>();
        for (Events events:eventsList) {
            EventsVO eventsVO = new EventsVO();
            BeanUtils.copyProperties(events,eventsVO);
            eventsVOList.add(eventsVO);
        }
        return eventsVOList;
    }

    @Override
    public List<Events> findAllEvents(String eventsTo) {
        return repository.findAllByEventsTo(eventsTo);
    }

    @Override
    public Events findByEventsId(String eventsId) {
        return repository.findByEventsId(eventsId);
    }
}
