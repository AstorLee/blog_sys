package com.example.blog.sys.repository;

import com.example.blog.sys.dao.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/9/11 15:36
 **/

@Repository
public interface CommentRepository  extends JpaRepository<Comment,String> {

    List<Comment> findAllByCommentToUserId(String commentTo);

    List<Comment> findAllByArticleId(String articleId);

    List<Comment> findAllByFocusId(String focusId);

    Comment findByCommentId(String commentId);
}
