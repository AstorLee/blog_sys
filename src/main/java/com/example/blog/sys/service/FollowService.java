package com.example.blog.sys.service;

import com.example.blog.sys.dao.Follow;
import com.example.blog.sys.vo.FollowVO;

import java.util.List;

/**
 * @Author: LetBeing
 * @Description :
 * @Date: 2019/8/30 14:55
 **/

public interface FollowService {

    Boolean delFollow(String id);

    Boolean saveFollow(Follow follow);

    List<FollowVO> findAllByFollowTypeAndFollowFrom(String type, String userId);

    List<FollowVO> findAllByFollowType(String type);

    Follow findByFollowId(String followId);

    Follow findByFollowFromAndFollowTO(String follow,String to);

    List<Follow> findAllByFollowTo(String to);
}
